export const SOCIALS = {
  fb: {
    link: 'https://www.facebook.com/codemosgame/',
  },
  twitter: {
    link: 'https://twitter.com/taddei_julia'
  },
  democraticCenter: {
    link: 'http://www.democratic.center/',
  },
  instagram: {
    link: 'https://www.instagram.com/codemos/'
  },
  youTube: {
    link: 'https://www.youtube.com/channel/UCrcrXolByt3ULBDpvqqMHbw'
  }
};
