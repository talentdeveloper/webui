import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error404ActivityComponent } from './activities/error-404/error-404.activity';
import { Error500ActivityComponent } from './activities/error-500/error-500.activity';
import { NotImplementedActivityComponent } from './activities/not-implemented/not-implemented.activity';
import { SocialComponent } from './components/social/social.component';
import { AboutGameActivityComponent } from './activities/about-game/about-game.activity';
import { GameRulesActivityComponent } from './activities/game-rules/game-rules.activity';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AttributionActivityComponent } from './activities/attribution/attribution.activity';
import { PartnersActivityComponent } from './activities/partners/partners.activity';
import { WelcomeActivityComponent } from './activities/welcome/welcome.activity';

const routes: Routes = [
  {path: '404', component: Error404ActivityComponent},
  {path: '500', component: Error500ActivityComponent},
  {path: 'about', component: AboutGameActivityComponent},
  {path: 'rules', component: GameRulesActivityComponent},
  {path: 'attributions', component: AttributionActivityComponent},
  {path: 'partners', component: PartnersActivityComponent},
  {path: 'welcome', component: WelcomeActivityComponent},
  // {path: '**', redirectTo: '/404'},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    TranslateModule.forChild()
  ],
  declarations: [
    Error404ActivityComponent,
    Error500ActivityComponent,
    NotImplementedActivityComponent,
    SocialComponent,
    AboutGameActivityComponent,
    GameRulesActivityComponent,
    AttributionActivityComponent,
    PartnersActivityComponent,
    WelcomeActivityComponent,
  ],
})
export class StaticModule {
}
