import { Component } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-not-implemented',
  templateUrl: 'not-implemented.activity.html',
  styleUrls: ['not-implemented.activity.css'],
})
export class NotImplementedActivityComponent extends BaseStaticComponent {
  constructor(
    translate: TranslateService,
    sessionService: SessionService,
  ) {
    super(translate, sessionService);
  }
}
