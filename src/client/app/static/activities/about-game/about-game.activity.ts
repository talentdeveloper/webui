import { Component } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-about-game',
  templateUrl: 'about-game.activity.html',
  styleUrls: ['about-game.activity.css'],
})
export class AboutGameActivityComponent extends BaseStaticComponent {
  constructor(
    translate: TranslateService,
    sessionService: SessionService,
  ) {
    super(translate, sessionService);
  }
}
