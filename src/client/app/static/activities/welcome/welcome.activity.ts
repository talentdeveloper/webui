import { Component, OnInit } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';
import { Router } from '@angular/router';
import { ROUTES } from '../../../shared/constants/routes';
import { CookieService } from '../../../shared/services/cookie.service';
import { COOKIES } from '../../../shared/constants/cookieNames';
import { RedirectService } from '../../../shared/services/redirect.service';

@Component({
  moduleId: module.id,
  selector: 'cd-welcome',
  templateUrl: 'welcome.activity.html',
  styleUrls: ['welcome.activity.css'],
})
export class WelcomeActivityComponent extends BaseStaticComponent {
  constructor(
    translate: TranslateService,
    sessionService: SessionService,
    private router: Router,
    private cookieService: CookieService,
    private redirectService: RedirectService,
  ) {
    super(translate, sessionService);
  }

  public goToMapView() {
    this.cookieService.set(COOKIES.visitedWelcomePage, true);
    const redirectUrl = this.redirectService.getRedirectData().redirectUrl || ROUTES.home;
    this.redirectService.setRedirectUrl(null);
    this.router.navigate([redirectUrl]);
  }
}
