import { Component } from '@angular/core';
import { SOCIALS } from '../../constants/socials';

@Component({
  moduleId: module.id,
  selector: 'cd-social',
  templateUrl: 'social.component.html',
  styleUrls: ['social.component.css'],
})
export class SocialComponent {
  public SOCIALS = SOCIALS;
}
