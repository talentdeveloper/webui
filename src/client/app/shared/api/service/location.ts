import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class LocationApi extends BaseApi {


  /**
   * Get Nearby Locations
   * @remarks Operation handler for getLocations
   * @param { number } latitude  * @param { number } longitude  * @param { number } radius
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getLocations(latitude: number, longitude: number, radius: number, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<LocationInfo>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/location`);
    if (latitude) {
      queryParams['latitude'] = String(latitude);
    }

    if (longitude) {
      queryParams['longitude'] = String(longitude);
    }

    if (radius) {
      queryParams['radius'] = String(radius);
    }

    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, false);
  }


  /**
   * Add location
   * @remarks Operation handler for addLocation
   * @param { Point } saveLocation
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public addLocation(saveLocation: Point, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Location> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/location`);


    if (saveLocation) {
      rqBuilder.withJsonBody(saveLocation);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get Location
   * @remarks Operation handler for getLocation
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getLocation(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Location> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/location/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Update location
   * @remarks Operation handler for updateLocation
   * @param { string } id  * @param { Point } saveLocation
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public updateLocation(id: string, saveLocation: Point, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Location> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('put')
                  .withPath(`/location/${id}`);

    pathParams['id'] = String(id);


    if (saveLocation) {
      rqBuilder.withJsonBody(saveLocation);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Remove location
   * @remarks Operation handler for removeLocation
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public removeLocation(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('delete')
                  .withPath(`/location/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get Nearest Location
   * @remarks Operation handler for getNearestLocation
   * @param { FindNearestLocationRequest } visitedLocations
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getNearestLocation(visitedLocations: FindNearestLocationRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Location> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/location/nearest`);


    if (visitedLocations) {
      rqBuilder.withJsonBody(visitedLocations);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}

import { LocationInfo } from '../models/location-info.models';
import { Point } from '../models/point.model';
import { Location } from '../models/location.models';
import { FindNearestLocationRequest } from '../models/findnearestlocationrequest';
