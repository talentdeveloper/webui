import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';
import { ProjectType } from '../models/project-type.model';
import { ProjectCategory } from '../models/project-category.model';

@Injectable()
export class ProjectCategoryApi extends BaseApi {

  public getProjectCategories(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<ProjectCategory>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('get')
      .withPath(`/category`);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

  public getProjectCategory(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<ProjectCategory> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('get')
      .withPath(`/category/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }
}
