import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';
import { OAuthToken } from './oAuthToken';

export class OAuth1SocialRequest extends BaseDto {
  private _token: OAuthToken;
  private _oAuthVerifier: string;

  constructor(input: any) {
    super();

    if (!input) {
      return;
    }

    input = super.fromCamelCaseToSnakeCase(input);
    this.token = input['token'] ? new OAuthToken(input['token']) : null;
    this.oAuthVerifier = input['oauth_verifier'];
  }

  toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['token'] = this.token.toJSON();
    result['oauth_verifier'] = this.oAuthVerifier;
    return result;
  }

  get token(): OAuthToken {
    return this._token;
  }

  set token(value: OAuthToken) {
    this._token = value;
  }

  get oAuthVerifier(): string {
    return this._oAuthVerifier;
  }

  set oAuthVerifier(value: string) {
    this._oAuthVerifier = value;
  }
}
