import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';

export class OAuthToken extends BaseDto {
  private _oauthToken: string;
  private _oauthTokenSecret: string;

  constructor(input?: any) {
    super();

    if (!input) {
      return;
    }

    input = super.fromCamelCaseToSnakeCase(input);
    this.oauthToken = input['oauth_token'];
    this.oauthTokenSecret = input['oauth_token_secret'];
  }

  toJSON(): Dictionary<any> {
    const dto: Dictionary<any> = {};
    dto['oauth_token'] = this.oauthToken;
    dto['oauth_token_secret'] = this.oauthTokenSecret;
    return dto;
  }

  get oauthTokenSecret(): string {
    return this._oauthTokenSecret;
  }

  set oauthTokenSecret(value: string) {
    this._oauthTokenSecret = value;
  }

  get oauthToken(): string {
    return this._oauthToken;
  }

  set oauthToken(value: string) {
    this._oauthToken = value;
  }
}
