import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class User extends BaseDto {
  private _id: string;
  private _email: string;
  private _first_name: string;
  private _last_name: string;
  private _birth_date: string;
  private _gender: string;
  private _city: string;
  private _use_notifications: boolean;
  private _language: string;
  private _address: string;
  private _score: number;
  private _level: number;
  private _title: string;
  private _trophies: number;
  private _has_password: boolean;

  /**
   * Initialize a new instance of User
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.email = input['email'];
      this.firstName = input['first_name'];
      this.lastName = input['last_name'];
      this.birthDate = input['birth_date'];
      this.gender = input['gender'];
      this.city = input['city'];
      this.useNotifications = input['use_notifications'];
      this.language = input['language'];
      this.address = input['address'];
      this.score = input['score'];
      this.level = input['level'];
      this.title = input['title'];
      this.trophies = input['trophies'];
      this.hasPassword = input['has_password'];
  }

  /**
   * Completely clone this instance.
   * @returns User - Cloned object.
   **/
  public clone(): User {
    return new User(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['email'] = this.email;
    result['first_name'] = this.firstName;
    result['last_name'] = this.lastName;
    result['birth_date'] = this.birthDate;
    result['gender'] = this.gender;
    result['city'] = this.city;
    result['use_notifications'] = this.useNotifications;
    result['language'] = this.language;
    result['address'] = this.address;
    result['score'] = this.score;
    result['level'] = this.level;
    result['title'] = this.title;
    result['trophies'] = this.trophies;
    result['has_password'] = this.hasPassword;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of email
  * @returns - Current value of email.
  **/
  get email(): string {
    return this._email;
  }

  /**
  * Change the value of email.
  * @param newVal - New value to assign.
  **/
  set email(newVal: string) {
    this._email = newVal.trim().toLowerCase();
  }

  /**
  * Get value of first_name
  * @returns - Current value of first_name.
  **/
  get firstName(): string {
    return this._first_name;
  }

  /**
  * Change the value of first_name.
  * @param newVal - New value to assign.
  **/
  set firstName(newVal: string) {
    this._first_name = newVal;
  }

  /**
  * Get value of last_name
  * @returns - Current value of last_name.
  **/
  get lastName(): string {
    return this._last_name;
  }

  /**
  * Change the value of last_name.
  * @param newVal - New value to assign.
  **/
  set lastName(newVal: string) {
    this._last_name = newVal;
  }

  /**
  * Get value of birth_date
  * @returns - Current value of birth_date.
  **/
  get birthDate(): string {
    return this._birth_date;
  }

  /**
  * Change the value of birth_date.
  * @param newVal - New value to assign.
  **/
  set birthDate(newVal: string) {
    this._birth_date = newVal;
  }

  /**
  * Get value of gender
  * @returns - Current value of gender.
  **/
  get gender(): string {
    return this._gender;
  }

  /**
  * Change the value of gender.
  * @param newVal - New value to assign.
  **/
  set gender(newVal: string) {
    this._gender = newVal;
  }

  /**
  * Get value of City
  * @returns - Current value of City.
  **/
  get city(): string {
    return this._city;
  }

  /**
  * Change the value of City.
  * @param newVal - New value to assign.
  **/
  set city(newVal: string) {
    this._city = newVal;
  }

  /**
  * Get value of use_notifications
  * @returns - Current value of use_notifications.
  **/
  get useNotifications(): boolean {
    return this._use_notifications;
  }

  /**
  * Change the value of use_notifications.
  * @param newVal - New value to assign.
  **/
  set useNotifications(newVal: boolean) {
    this._use_notifications = newVal;
  }

  /**
  * Get value of language
  * @returns - Current value of language.
  **/
  get language(): string {
    return this._language;
  }

  /**
  * Change the value of language.
  * @param newVal - New value to assign.
  **/
  set language(newVal: string) {
    this._language = newVal;
  }

  /**
  * Get value of address
  * @returns - Current value of address.
  **/
  get address(): string {
    return this._address;
  }

  /**
  * Change the value of address.
  * @param newVal - New value to assign.
  **/
  set address(newVal: string) {
    this._address = newVal;
  }

  /**
  * Get value of score
  * @returns - Current value of score.
  **/
  get score(): number {
    return this._score;
  }

  /**
  * Change the value of score.
  * @param newVal - New value to assign.
  **/
  set score(newVal: number) {
    this._score = newVal;
  }

  /**
  * Get value of level
  * @returns - Current value of level.
  **/
  get level(): number {
    return this._level;
  }

  /**
  * Change the value of level.
  * @param newVal - New value to assign.
  **/
  set level(newVal: number) {
    this._level = newVal;
  }

  /**
  * Get value of title
  * @returns - Current value of title.
  **/
  get title(): string {
    return this._title;
  }

  /**
  * Change the value of title.
  * @param newVal - New value to assign.
  **/
  set title(newVal: string) {
    this._title = newVal;
  }

  /**
  * Get value of trophies
  * @returns - Current value of trophies.
  **/
  get trophies(): number {
    return this._trophies;
  }

  /**
  * Change the value of trophies.
  * @param newVal - New value to assign.
  **/
  set trophies(newVal: number) {
    this._trophies = newVal;
  }

  get hasPassword(): boolean {
    return this._has_password;
  }

  set hasPassword(newVal: boolean) {
    this._has_password = newVal;
  }
}

