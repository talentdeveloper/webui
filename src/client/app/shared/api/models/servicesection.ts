import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ServiceSection extends BaseDto {
  private _successful: boolean;
  private _node_id: string;
  private _api_version: string;
  private _error_code: any;
  private _error_message: any;
  private _validation_errors: string;
  private _verbose_error_message: string;
  private _reward: number;
  private _title: string;

  /**
   * Initialize a new instance of ServiceSection
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.successful = input['successful'];
      this.nodeId = input['node_id'];
      this.apiVersion = input['api_version'];
      this.errorCode = input['error_code'];
      this.errorMessage = input['error_message'];
      this.validationErrors = input['validation_errors'];
      this.verboseErrorMessage = input['verbose_error_message'];
      this.reward = input['reward'];
      this.title = input['title'];
  }

  /**
   * Completely clone this instance.
   * @returns ServiceSection - Cloned object.
   **/
  public clone(): ServiceSection {
    return new ServiceSection(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['successful'] = this.successful;
    result['node_id'] = this.nodeId;
    result['api_version'] = this.apiVersion;
    result['error_code'] = this.errorCode;
    result['error_message'] = this.errorMessage;
    result['validation_errors'] = this.validationErrors;
    result['verbose_error_message'] = this.verboseErrorMessage;
    result['reward'] = this.reward;
    result['title'] = this.title;
    return result;
  }

  /**
  * Flag which indicates whether requested operation was successful or not
  * @returns - Current value of successful.
  **/
  get successful(): boolean {
    return this._successful;
  }

  /**
  * Change the value of successful.
  * @param newVal - New value to assign.
  **/
  set successful(newVal: boolean) {
    this._successful = newVal;
  }

  /**
  * Unique identifier of server node which generated this response
  * @returns - Current value of node_id.
  **/
  get nodeId(): string {
    return this._node_id;
  }

  /**
  * Change the value of node_id.
  * @param newVal - New value to assign.
  **/
  set nodeId(newVal: string) {
    this._node_id = newVal;
  }

  /**
  * API version that is currently deployed on &#x60;node_id&#x60; server instance
  * @returns - Current value of api_version.
  **/
  get apiVersion(): string {
    return this._api_version;
  }

  /**
  * Change the value of api_version.
  * @param newVal - New value to assign.
  **/
  set apiVersion(newVal: string) {
    this._api_version = newVal;
  }

  /**
  * See error codes glosarry
  * @returns - Current value of error_code.
  **/
  get errorCode(): any {
    return this._error_code;
  }

  /**
  * Change the value of error_code.
  * @param newVal - New value to assign.
  **/
  set errorCode(newVal: any) {
    this._error_code = newVal;
  }

  /**
  * Generic error model (not localized), use only as hint
  * @returns - Current value of error_message.
  **/
  get errorMessage(): any {
    return this._error_message;
  }

  /**
  * Change the value of error_message.
  * @param newVal - New value to assign.
  **/
  set errorMessage(newVal: any) {
    this._error_message = newVal;
  }

  /**
  * Errors happened during validation
  * @returns - Current value of validation_errors.
  **/
  get validationErrors(): string {
    return this._validation_errors;
  }

  /**
  * Change the value of validation_errors.
  * @param newVal - New value to assign.
  **/
  set validationErrors(newVal: string) {
    this._validation_errors = newVal;
  }

  /**
  * Get value of verbose_error_message
  * @returns - Current value of verbose_error_message.
  **/
  get verboseErrorMessage(): string {
    return this._verbose_error_message;
  }

  /**
  * Change the value of verbose_error_message.
  * @param newVal - New value to assign.
  **/
  set verboseErrorMessage(newVal: string) {
    this._verbose_error_message = newVal;
  }

  /**
  * Get value of reward
  * @returns - Current value of reward.
  **/
  get reward(): number {
    return this._reward;
  }

  /**
  * Change the value of reward.
  * @param newVal - New value to assign.
  **/
  set reward(newVal: number) {
    this._reward = newVal;
  }

  /**
  * Get value of title
  * @returns - Current value of title.
  **/
  get title(): string {
    return this._title;
  }

  /**
  * Change the value of title.
  * @param newVal - New value to assign.
  **/
  set title(newVal: string) {
    this._title = newVal;
  }


}

