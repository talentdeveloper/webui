import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ProjectImpact extends BaseDto {
  private _co2_reduction: number;
  private _live_hours_saved: number;
  private _jobs_created: number;
  private _sdg: Array<number>;

  /**
   * Initialize a new instance of Impact
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.co2Reduction = input['co2_reduction'];
      this.liveHoursSaved = input['live_hours_saved'];
      this.jobsCreated = input['jobs_created'];
      this.sdg = input['sdg'];
  }

  /**
   * Completely clone this instance.
   * @returns ProjectImpact - Cloned object.
   **/
  public clone(): ProjectImpact {
    return new ProjectImpact(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['co2_reduction'] = this.co2Reduction;
    result['live_hours_saved'] = this.liveHoursSaved;
    result['jobs_created'] = this.jobsCreated;
    result['sdg'] = this.sdg;
    return result;
  }

  /**
  * Get value of co2_reduction
  * @returns - Current value of co2_reduction.
  **/
  get co2Reduction(): number {
    return this._co2_reduction;
  }

  /**
  * Change the value of co2_reduction.
  * @param newVal - New value to assign.
  **/
  set co2Reduction(newVal: number) {
    this._co2_reduction = newVal;
  }

  /**
  * Get value of live_hours_saved
  * @returns - Current value of live_hours_saved.
  **/
  get liveHoursSaved(): number {
    return this._live_hours_saved;
  }

  /**
  * Change the value of live_hours_saved.
  * @param newVal - New value to assign.
  **/
  set liveHoursSaved(newVal: number) {
    this._live_hours_saved = newVal;
  }

  /**
  * Get value of jobs_created
  * @returns - Current value of jobs_created.
  **/
  get jobsCreated(): number {
    return this._jobs_created;
  }

  /**
  * Change the value of jobs_created.
  * @param newVal - New value to assign.
  **/
  set jobsCreated(newVal: number) {
    this._jobs_created = newVal;
  }

  /**
  * Get value of sdg
  * @returns - Current value of sdg.
  **/
  get sdg(): Array<number> {
    return this._sdg;
  }

  /**
  * Change the value of sdg.
  * @param newVal - New value to assign.
  **/
  set sdg(newVal: Array<number>) {
    this._sdg = newVal;
  }


}

