import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ProjectCategory extends BaseDto {
  private _id: string;
  private _name: string;
  private _type: string;

  constructor(input?: any) {
    super();
    // Skip if no input
    if (!input) {
      return;
    }
    input = super.fromCamelCaseToSnakeCase(input);
    this.id = input['id'];
    this.type = input['type'];
    this.name = input['name'];
  }

  public clone(): ProjectCategory {
    return new ProjectCategory(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['type'] = this.type;
    result['name'] = this.name;
    return result;
  }


  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }
}

