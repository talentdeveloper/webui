import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class FindNearestLocationRequest extends BaseDto {
  private _camera: Point;
  private _visited_locations: Array<string>;

  /**
   * Initialize a new instance of FindNearestLocationRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.camera = input['camera'] ? new Point(input['camera']) : null;
      this.visitedLocations = input['visited_locations'];
  }

  /**
   * Completely clone this instance.
   * @returns FindNearestLocationRequest - Cloned object.
   **/
  public clone(): FindNearestLocationRequest {
    return new FindNearestLocationRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['camera'] = this.camera ? this.camera.toJSON() : null;
    result['visited_locations'] = this.visitedLocations;
    return result;
  }

  /**
  * Get value of camera
  * @returns - Current value of camera.
  **/
  get camera(): Point {
    return this._camera;
  }

  /**
  * Change the value of camera.
  * @param newVal - New value to assign.
  **/
  set camera(newVal: Point) {
    this._camera = newVal;
  }

  /**
  * Get value of visited_locations
  * @returns - Current value of visited_locations.
  **/
  get visitedLocations(): Array<string> {
    return this._visited_locations;
  }

  /**
  * Change the value of visited_locations.
  * @param newVal - New value to assign.
  **/
  set visitedLocations(newVal: Array<string>) {
    this._visited_locations = newVal;
  }


}

import { Point } from './point.model';
