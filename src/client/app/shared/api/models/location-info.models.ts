import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class LocationInfo extends BaseDto {
  private _id: string;
  private _address: string;
  private _type: string;
  private _locked: boolean;
  private _coordinates: Point;

  /**
   * Initialize a new instance of LocationInfo
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.type = input['type'];
      this.address = input['address'];
      this.locked = input['locked'];
      this.coordinates = input['coordinates'] ? new Point(input['coordinates']) : null;
  }

  /**
   * Completely clone this instance.
   * @returns LocationInfo - Cloned object.
   **/
  public clone(): LocationInfo {
    return new LocationInfo(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['type'] = this.type;
    result['address'] = this.address;
    result['locked'] = this.locked;
    result['coordinates'] = this.coordinates ? this.coordinates.toJSON() : null;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of address
  * @returns - Current value of address.
  **/
  get address(): string {
    return this._address;
  }

  /**
  * Change the value of address.
  * @param newVal - New value to assign.
  **/
  set address(newVal: string) {
    this._address = newVal;
  }

  /**
  * Get value of locked
  * @returns - Current value of locked.
  **/
  get locked(): boolean {
    return this._locked;
  }

  /**
  * Change the value of locked.
  * @param newVal - New value to assign.
  **/
  set locked(newVal: boolean) {
    this._locked = newVal;
  }

  /**
  * Get value of coordinates
  * @returns - Current value of coordinates.
  **/
  get coordinates(): Point {
    return this._coordinates;
  }

  /**
  * Change the value of coordinates.
  * @param newVal - New value to assign.
  **/
  set coordinates(newVal: Point) {
    this._coordinates = newVal;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }
}

import { Point } from './point.model';
