import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class Version extends BaseDto {
  private _version: string;
  private _api_version: string;
  private _build_date: string;
  private _build_number: string;
  private _vcs_id: string;

  /**
   * Initialize a new instance of Version
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.version = input['version'];
      this.apiVersion = input['api_version'];
      this.buildDate = input['build_date'];
      this.buildNumber = input['build_number'];
      this.vcsId = input['vcs_id'];
  }

  /**
   * Completely clone this instance.
   * @returns Version - Cloned object.
   **/
  public clone(): Version {
    return new Version(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['version'] = this.version;
    result['api_version'] = this.apiVersion;
    result['build_date'] = this.buildDate;
    result['build_number'] = this.buildNumber;
    result['vcs_id'] = this.vcsId;
    return result;
  }

  /**
  * Get value of version
  * @returns - Current value of version.
  **/
  get version(): string {
    return this._version;
  }

  /**
  * Change the value of version.
  * @param newVal - New value to assign.
  **/
  set version(newVal: string) {
    this._version = newVal;
  }

  /**
  * Get value of api_version
  * @returns - Current value of api_version.
  **/
  get apiVersion(): string {
    return this._api_version;
  }

  /**
  * Change the value of api_version.
  * @param newVal - New value to assign.
  **/
  set apiVersion(newVal: string) {
    this._api_version = newVal;
  }

  /**
  * Get value of build_date
  * @returns - Current value of build_date.
  **/
  get buildDate(): string {
    return this._build_date;
  }

  /**
  * Change the value of build_date.
  * @param newVal - New value to assign.
  **/
  set buildDate(newVal: string) {
    this._build_date = newVal;
  }

  /**
  * Get value of build_number
  * @returns - Current value of build_number.
  **/
  get buildNumber(): string {
    return this._build_number;
  }

  /**
  * Change the value of build_number.
  * @param newVal - New value to assign.
  **/
  set buildNumber(newVal: string) {
    this._build_number = newVal;
  }

  /**
  * Get value of vcs_id
  * @returns - Current value of vcs_id.
  **/
  get vcsId(): string {
    return this._vcs_id;
  }

  /**
  * Change the value of vcs_id.
  * @param newVal - New value to assign.
  **/
  set vcsId(newVal: string) {
    this._vcs_id = newVal;
  }


}

