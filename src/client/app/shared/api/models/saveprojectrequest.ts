import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';
import { AbstractSaveProjectRequest } from '../common/AbstractSaveProjectRequest';


export class SaveProjectRequest extends BaseDto implements AbstractSaveProjectRequest {
  private _name: string;
  private _description: string;
  private _location: string;
  private _category: string;
  private _capacity: number;

  /**
   * Initialize a new instance of SaveProjectRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.name = input['name'];
      this.description = input['description'];
      this.location = input['location'];
      this.category = input['category'];
      this.capacity = input['capacity'];
  }

  /**
   * Completely clone this instance.
   * @returns SaveProjectRequest - Cloned object.
   **/
  public clone(): SaveProjectRequest {
    return new SaveProjectRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['name'] = this.name;
    result['description'] = this.description;
    result['location'] = this.location;
    result['category'] = this.category;
    result['capacity'] = this.capacity;
    return result;
  }

  /**
  * Get value of name
  * @returns - Current value of name.
  **/
  get name(): string {
    return this._name;
  }

  /**
  * Change the value of name.
  * @param newVal - New value to assign.
  **/
  set name(newVal: string) {
    this._name = newVal;
  }

  /**
  * Get value of description
  * @returns - Current value of description.
  **/
  get description(): string {
    return this._description;
  }

  /**
  * Change the value of description.
  * @param newVal - New value to assign.
  **/
  set description(newVal: string) {
    this._description = newVal;
  }

  /**
  * Get value of location
  * @returns - Current value of location.
  **/
  get location(): string {
    return this._location;
  }

  /**
  * Change the value of location.
  * @param newVal - New value to assign.
  **/
  set location(newVal: string) {
    this._location = newVal;
  }

  /**
  * Get value of category
  * @returns - Current value of category.
  **/
  get category(): string {
    return this._category;
  }

  /**
  * Change the value of category.
  * @param newVal - New value to assign.
  **/
  set category(newVal: string) {
    this._category = newVal;
  }

  /**
  * Get value of capacity
  * @returns - Current value of capacity.
  **/
  get capacity(): number {
    return this._capacity;
  }

  /**
  * Change the value of capacity.
  * @param newVal - New value to assign.
  **/
  set capacity(newVal: number) {
    this._capacity = newVal;
  }


}

