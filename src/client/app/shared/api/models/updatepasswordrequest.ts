import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class UpdatePasswordRequest extends BaseDto {
  private _password: string;
  private _new_password: string;

  /**
   * Initialize a new instance of UpdatePasswordRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.password = input['password'];
      this.newPassword = input['new_password'];
  }

  /**
   * Completely clone this instance.
   * @returns UpdatePasswordRequest - Cloned object.
   **/
  public clone(): UpdatePasswordRequest {
    return new UpdatePasswordRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['password'] = this.password;
    result['new_password'] = this.newPassword;
    return result;
  }

  /**
  * Get value of password
  * @returns - Current value of password.
  **/
  get password(): string {
    return this._password;
  }

  /**
  * Change the value of password.
  * @param newVal - New value to assign.
  **/
  set password(newVal: string) {
    this._password = newVal;
  }

  /**
  * Get value of new_password
  * @returns - Current value of new_password.
  **/
  get newPassword(): string {
    return this._new_password;
  }

  /**
  * Change the value of new_password.
  * @param newVal - New value to assign.
  **/
  set newPassword(newVal: string) {
    this._new_password = newVal;
  }


}

