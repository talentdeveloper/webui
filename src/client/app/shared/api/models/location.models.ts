import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class Location extends BaseDto {
  private _id: string;
  private _type: string;
  private _address: string;
  private _locked: boolean;
  private _creator: Author;
  private _coordinates: Point;
  private _projects: Array<ProjectMinimalData>;
  private _events: Array<ProjectMinimalData>;
  private _messages: Array<ProjectMinimalData>;

  /**
   * Initialize a new instance of Location
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.type = input['type'];
      this.address = input['address'];
      this.locked = input['locked'];
      this.creator = input['creator'] ? new Author(input['creator']) : null;
      this.coordinates = input['coordinates'] ? new Point(input['coordinates']) : null;
      this.projects = input['projects'] ? input['projects'].map((project: ProjectMinimalData) => new ProjectMinimalData(project)) : null;
      this.events = input['events'] ? input['events'].map((event: ProjectMinimalData) => new ProjectMinimalData(event)) : null;
      this.messages = input['messages'] ? input['messages'].map((message: ProjectMinimalData) => new ProjectMinimalData(message)) : null;
  }

  /**
   * Completely clone this instance.
   * @returns Location - Cloned object.
   **/
  public clone(): Location {
    return new Location(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['address'] = this.address;
    result['type'] = this.type;
    result['locked'] = this.locked;
    result['creator'] = this.creator ? this.creator.toJSON() : null;
    result['coordinates'] = this.coordinates ? this.coordinates.toJSON() : null;
    result['projects'] = this.projects;
    result['events'] = this.events;
    result['messages'] = this.messages;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of address
  * @returns - Current value of address.
  **/
  get address(): string {
    return this._address;
  }

  /**
  * Change the value of address.
  * @param newVal - New value to assign.
  **/
  set address(newVal: string) {
    this._address = newVal;
  }

  /**
  * Get value of locked
  * @returns - Current value of locked.
  **/
  get locked(): boolean {
    return this._locked;
  }

  /**
  * Change the value of locked.
  * @param newVal - New value to assign.
  **/
  set locked(newVal: boolean) {
    this._locked = newVal;
  }

  /**
  * Get value of creator
  * @returns - Current value of creator.
  **/
  get creator(): Author {
    return this._creator;
  }

  /**
  * Change the value of creator.
  * @param newVal - New value to assign.
  **/
  set creator(newVal: Author) {
    this._creator = newVal;
  }

  /**
  * Get value of coordinates
  * @returns - Current value of coordinates.
  **/
  get coordinates(): Point {
    return this._coordinates;
  }

  /**
  * Change the value of coordinates.
  * @param newVal - New value to assign.
  **/
  set coordinates(newVal: Point) {
    this._coordinates = newVal;
  }

  /**
  * Get value of projects
  * @returns - Current value of projects.
  **/
  get projects(): Array<ProjectMinimalData> {
    return this._projects;
  }

  /**
  * Change the value of projects.
  * @param newVal - New value to assign.
  **/
  set projects(newVal: Array<ProjectMinimalData>) {
    this._projects = newVal;
  }

  /**
  * Get value of events
  * @returns - Current value of events.
  **/
  get events(): Array<ProjectMinimalData> {
    return this._events;
  }

  /**
  * Change the value of events.
  * @param newVal - New value to assign.
  **/
  set events(newVal: Array<ProjectMinimalData>) {
    this._events = newVal;
  }

  /**
  * Get value of messages
  * @returns - Current value of messages.
  **/
  get messages(): Array<ProjectMinimalData> {
    return this._messages;
  }

  /**
  * Change the value of messages.
  * @param newVal - New value to assign.
  **/
  set messages(newVal: Array<ProjectMinimalData>) {
    this._messages = newVal;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }
}

import { Author } from './author';
import { Point } from './point.model';
import { ProjectMinimalData } from './projectminimaldata';
