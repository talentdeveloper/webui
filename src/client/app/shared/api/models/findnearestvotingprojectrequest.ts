import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class FindNearestVotingProjectRequest extends BaseDto {
  private _camera: Point;
  private _projects_visited: Array<string>;

  /**
   * Initialize a new instance of FindNearestVotingProjectRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.camera = input['camera'] ? new Point(input['camera']) : null;
      this.projectsVisited = input['projects_visited'];
  }

  /**
   * Completely clone this instance.
   * @returns FindNearestVotingProjectRequest - Cloned object.
   **/
  public clone(): FindNearestVotingProjectRequest {
    return new FindNearestVotingProjectRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['camera'] = this.camera ? this.camera.toJSON() : null;
    result['projects_visited'] = this.projectsVisited;
    return result;
  }

  /**
  * Get value of camera
  * @returns - Current value of camera.
  **/
  get camera(): Point {
    return this._camera;
  }

  /**
  * Change the value of camera.
  * @param newVal - New value to assign.
  **/
  set camera(newVal: Point) {
    this._camera = newVal;
  }

  /**
  * Get value of projects_visited
  * @returns - Current value of projects_visited.
  **/
  get projectsVisited(): Array<string> {
    return this._projects_visited;
  }

  /**
  * Change the value of projects_visited.
  * @param newVal - New value to assign.
  **/
  set projectsVisited(newVal: Array<string>) {
    this._projects_visited = newVal;
  }


}

import { Point } from './point.model';
