import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ProjectMinimalData extends BaseDto {
  private _id: string;
  private _name: string;
  private _won_voting: boolean;

  /**
   * Initialize a new instance of ProjectMinimalData
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.name = input['name'];
      this.wonVoting = input['won_voting'];
  }

  /**
   * Completely clone this instance.
   * @returns ProjectMinimalData - Cloned object.
   **/
  public clone(): ProjectMinimalData {
    return new ProjectMinimalData(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['name'] = this.name;
    result['won_voting'] = this.wonVoting;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of name
  * @returns - Current value of name.
  **/
  get name(): string {
    return this._name;
  }

  /**
  * Change the value of name.
  * @param newVal - New value to assign.
  **/
  set name(newVal: string) {
    this._name = newVal;
  }

  /**
  * Get value of won_voting
  * @returns - Current value of won_voting.
  **/
  get wonVoting(): boolean {
    return this._won_voting;
  }

  /**
  * Change the value of won_voting.
  * @param newVal - New value to assign.
  **/
  set wonVoting(newVal: boolean) {
    this._won_voting = newVal;
  }


}

