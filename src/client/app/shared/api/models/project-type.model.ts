import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ProjectType extends BaseDto {
  private _id: string;
  private _name: string;
  private _full_name: string;
  private _order: string;
  private _url: string;
  private _bg_url: string;
  private _color: string;
  private _disabled: boolean;

  constructor(input?: any) {
    super();
    // Skip if no input
    if (!input) {
      return;
    }
    input = super.fromCamelCaseToSnakeCase(input);
    this.id = input['id'];
    this.name = input['name'];
    this.url = input['url'];
    this.bgUrl = input['bg_url'];
    this.fullName = input['full_name'];
    this.order = input['order'];
    this.color = input['color'];
    this.disabled = input['disabled'] || false;
  }


  public clone(): ProjectType {
    return new ProjectType(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['name'] = this.name;
    result['url'] = this.url;
    result['bg_url'] = this.bgUrl;
    result['full_name'] = this.fullName;
    result['order'] = this.order;
    result['color'] = this.color;
    return result;
  }

  /**
   * Get value of id
   * @returns - Current value of id.
   **/
  get id(): string {
    return this._id;
  }

  /**
   * Change the value of id.
   * @param newVal - New value to assign.
   **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
   * Get value of name
   * @returns - Current value of name.
   **/
  get name(): string {
    return this._name;
  }

  /**
   * Change the value of name.
   * @param newVal - New value to assign.
   **/
  set name(newVal: string) {
    this._name = newVal;
  }

  /**
   * Get value of url
   * @returns - Current value of url.
   **/
  get url(): string {
    return this._url;
  }

  /**
   * Change the value of url.
   * @param newVal - New value to assign.
   **/
  set url(newVal: string) {
    this._url = newVal;
  }

  /**
   * Get value of bg_url
   * @returns - Current value of bg_url.
   **/
  get bgUrl(): string {
    return this._bg_url;
  }

  /**
   * Change the value of bg_url.
   * @param newVal - New value to assign.
   **/
  set bgUrl(newVal: string) {
    this._bg_url = newVal;
  }

  /**
   * Get value of color
   * @returns - Current value of color.
   **/
  get color(): string {
    return this._color;
  }

  /**
   * Change the value of color.
   * @param newVal - New value to assign.
   **/
  set color(newVal: string) {
    this._color = newVal;
  }

  /**
   * Get value of full name
   * @returns - Current value of full name.
   **/
  get fullName(): string {
    return this._full_name;
  }

  /**
   * Change the value of full name.
   * @param newVal - New value to assign.
   **/
  set fullName(newVal: string) {
    this._full_name = newVal;
  }

  /**
   * Get value of order
   * @returns - Current value of order.
   **/
  get order(): string {
    return this._order;
  }

  /**
   * Change the value of order.
   * @param newVal - New value to assign.
   **/
  set order(newVal: string) {
    this._order = newVal;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(newVal: boolean) {
    this._disabled = newVal;
  }
}
