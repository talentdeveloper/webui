import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';

export class SocialRequest extends BaseDto {
  private _token: string;

  /**
   * Initialize a new instance of UserSignupRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
  constructor(input?: any) {
    super();
    // Skip if no input
    if (!input) {
      return;
    }
    input = super.fromCamelCaseToSnakeCase(input);
    this.token = input['token'];
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['token'] = this.token;
    return result;
  }

  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }

}
