import { Config } from '../../config/env.config';
import { Observable } from 'rxjs/Observable';
import { ApiResponseObservable, Payload } from './ApiResponseDto';
import { Injectable } from '@angular/core';
import { Http, RequestMethod, Response } from '@angular/http';
import { ApiRequest } from './ApiRequest';
import { ApiError } from '../../models/errors';
import { ServiceSection } from '../models/servicesection';
import { BaseRequestBehavior } from './BaseRequestBehavior';
import { LoggingService } from '../../modules/utility/service/logging.service';
import { ApiResponse } from '../models/apiresponse';
import { BaseHandleApiErrors } from './BaseHandleApiErrors';
import { CUSTOM_HEADERS } from '../../constants/APIRelated';
import { NotificationService } from '../../components/notification/notification.service';
import { SessionService } from '../../services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { I18N_REWARDS } from '../../i18n/elements';
import { ViewModeService } from '../../services/view-mode.service';
import { RedirectService } from '../../services/redirect.service';
import { Router } from '@angular/router';

@Injectable()
export class BaseApi {
  protected apiBaseUrl: string;

  constructor(private http: Http,
              private baseRequest: BaseRequestBehavior,
              private loggingService: LoggingService,
              private baseHandleApiError: BaseHandleApiErrors,
              private notificationService: NotificationService,
              private translate: TranslateService,
              private redirectService: RedirectService,
              private router: Router,
              private logger: LoggingService,
              private viewModeService: ViewModeService,
              private sessionService: SessionService) {
    this.apiBaseUrl = Config.API;
  }

  protected performRequest<T extends Payload>(request: ApiRequest, withIndicator = true, strictAllow = false, usesThirdPartyApi = false): ApiResponseObservable<T> {
    if (!this.sessionService.isAuthorized() && request.method !== RequestMethod.Get && !strictAllow) {
      this.redirectService.setRedirectUrl(this.router.url);
      this.viewModeService.showViewModeDialog()
        .subscribe(() => {
          this.redirectService.setRedirectUrl(null);
        });
      return null;
    } else {
      this.baseRequest.launched(withIndicator);
      if (!usesThirdPartyApi) {
        request = this.transformRequest(request);
      }
      return this.http.request(request)
        .map(response => {
          const contentType = response.headers && response.headers.get('content-type');
          if (contentType && !contentType.startsWith('application/json')) {
            return response.arrayBuffer();
          }
          const jsonResponse = response.json();

          if (usesThirdPartyApi) {
            return jsonResponse;
          } else {
            if (jsonResponse.service && jsonResponse.service.successful) {
              // TODO check for trophy here

              const reward = jsonResponse.service.reward;
              if (!!reward) {
                const user = this.sessionService.getSecurityPrincipal();
                if (!!user) {
                  user.addPrincipalScore(reward);
                }
                this.translate.get([I18N_REWARDS.receive, I18N_REWARDS.lost])
                  .subscribe(res => {
                    if (reward > 0) {
                      this.notificationService.show(res[I18N_REWARDS.receive].replace('${reward}', reward));
                    } else {
                      this.notificationService.show(res[I18N_REWARDS.lost].replace('${reward}', Math.abs(reward)));
                    }
                  });
              }
              return jsonResponse;
            } else {
              let serviceSection = jsonResponse.service;
              if (!(serviceSection instanceof ServiceSection)) {
                serviceSection = new ServiceSection(serviceSection);
              }
              throw new ApiError(
                serviceSection,
                jsonResponse.payload
              );
            }
          }

        })
        .catch((err: ApiError | Response) => {
          if (err instanceof ApiError) {
            return this.handleError(err);
          } else if (err instanceof Response) {
            return this.handleHttpError(err);
          } else { // Something unexpected
            this.loggingService.error('Unexpected error occurred');
            this.loggingService.error(err);
            return Observable.throw(err);
          }
        })
        .finally(() => {
          this.baseRequest.completed();
        })
        .share();
    }
  }

  protected transformRequest(request: ApiRequest): ApiRequest {
    request.withCredentials = true;
    request.headers.set(CUSTOM_HEADERS.xRequestedBy.name, CUSTOM_HEADERS.xRequestedBy.value);
    return request;
  }

  protected handleHttpError(errorResponse: Response): Observable<any> {
    this.loggingService.error('Api error occurred:', errorResponse);
    const body = errorResponse.json();
    const errorBody: ApiResponse = new ApiResponse(body);
    const apiError = new ApiError(
      errorBody.service,
      errorResponse
    );
    return this.baseHandleApiError.handleGenericError(apiError);
  }

  protected handleError(response: ApiError): Observable<any> {
    this.loggingService.debug('Api returned unsuccessful response:', response);
    return this.baseHandleApiError.handleGenericError(response);
  }

}

