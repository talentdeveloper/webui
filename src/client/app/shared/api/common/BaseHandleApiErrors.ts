import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ApiError } from '../../models/errors';
import { Router, UrlSegmentGroup, UrlTree } from '@angular/router';
import { LoggingService } from '../../modules/utility/service/logging.service';
import { SessionService } from '../../services/session.service';
import { ROUTES } from '../../constants/routes';
import { ERROR_MESSAGES } from '../../constants/errors';
import { AuthService } from '../../../auth/services/auth.service';
import { RedirectService } from '../../services/redirect.service';
import { Subscription } from 'rxjs/Subscription';
import { Dictionary } from '../../types';
import { ViewModeService } from '../../services/view-mode.service';

@Injectable()
export class BaseHandleApiErrors {

  constructor(
    private router: Router,
    private logger: LoggingService,
    private sessionService: SessionService,
    private redirectService: RedirectService,
    private viewModeService: ViewModeService,
  ) {}

  public handleGenericError(error: ApiError): Observable<any> {
    // handling of generic error responses like http 401, 404 or 500
    if (error.status === 401) {
      this.sessionService.dropSession();
      if (this.getTargetStep(this.router)) {
        this.redirectService.setRedirectUrl(`/${this.getTargetStep(this.router)}`);
      }
    }
    if (error.status >= 500 && error.status < 0xF00) {
      this.router.navigate([ROUTES.internalServerError])
        .then(() => this.logger.log('navigated to 500 error page'))
        .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
    }
    return Observable.throw(error);
  }

  private getTargetStep(router: Router): string {
    let targetStep: string = null;
    const nextRoute: Subscription = router['navigations'].subscribe((value: Dictionary<any>) => {
      const rawUrl: UrlTree = value['rawUrl'];
      if (rawUrl.root.hasChildren()) {
        const children: UrlSegmentGroup = rawUrl.root.children['primary'];
        targetStep = children.segments[children.segments.length - 1].path;
        // this._backupTargetUrl = '/' + children.segments.join('/');
      }
    });
    nextRoute.unsubscribe();
    return targetStep;
  }

}

