import { LocationInfo } from '../models/location-info.models';
import { CreatorAware } from './CreatorAware';

export interface AbstractProject extends CreatorAware {

  id: string;
  name: string;
  description: string;
  location: LocationInfo;
  capacity: number;
  canVote: boolean;
  underVoting: boolean;
  wonVoting: boolean;
  votesReceived: number;

}

