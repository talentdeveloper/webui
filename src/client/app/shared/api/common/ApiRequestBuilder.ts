import { URLSearchParams, ResponseContentType } from '@angular/http';
import { ApiRequest } from './ApiRequest';
import { BaseDto } from './BaseDto';
import { Dictionary } from '../../types';

export class ApiRequestBuilder {
  protected baseUrl: string = null;
  protected method: string;
  protected path = '/';
  protected pathParams: Dictionary<string> = {};
  protected queryParams: Dictionary<string> = {};
  protected body = '';
  protected extraHeaders: Dictionary<string> = {};
  protected responseType: ResponseContentType;

  public constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  public build(): ApiRequest {
    const queryParameters = new URLSearchParams();

    for (const key in this.queryParams) {
      if (this.queryParams.hasOwnProperty(key)) {
        queryParameters.set(key, this.queryParams[key]);
      }
    }

    const request = new ApiRequest({
      url: this.baseUrl + this.buildPath(),
      method: this.method,
      body: this.body,
      search: queryParameters,
      responseType: this.responseType || ResponseContentType.Json,
    });

    request.headers.set('Content-Type', 'application/json');

    for (const key in this.extraHeaders) {
      if (this.extraHeaders.hasOwnProperty(key)) {
        request.headers.set(key, this.extraHeaders[key]);
      }
    }

    return request;
  }

  public withJsonBody(dto: BaseDto|Dictionary<any>|Array<BaseDto>): ApiRequestBuilder {
    if (dto instanceof BaseDto) {
      this.body = JSON.stringify(dto.toJSON());
    } else if (dto instanceof Array) {
      const body: Array<Dictionary<any>> = [];
      for (const item of dto) {
        body.push(item.toJSON());
      }
      this.body = JSON.stringify(body);
    } else {
      this.body = JSON.stringify(dto);
    }
    return this;
  }

  public withMethod(method: string) {
    this.method = method;
    return this;
  }

  public withPath(path: string) {
    this.path = path;
    return this;
  }

  public withPathParams(pathParams: any) {
    this.pathParams = pathParams;
    return this;
  }

  public withQueryParams(queryParams: Dictionary<string>) {
    this.queryParams = queryParams;
    return this;
  }

  public withExtraHeaders(extraHeaders: Dictionary<string>) {
    this.extraHeaders = extraHeaders;
    return this;
  }

  public withResponseType(responseType: ResponseContentType) {
    this.responseType = responseType;
    return this;
  }

  protected buildPath(): string {
    let path = this.path;
    for (const key in this.pathParams) {
      if (this.pathParams.hasOwnProperty(key)) {
        path = path.replace(':' + key, this.pathParams[key]);
      }
    }
    return path;
  }
}
