import { BaseDto } from './BaseDto';
import { Observable } from 'rxjs/Observable';
import { ServiceSection } from '../models/servicesection';

export type Payload = BaseDto|Array<BaseDto>|ArrayBuffer;

export class ApiResponseDto<T extends Payload> {
  public service: ServiceSection;
  public payload: T;
}

export type ApiResponseObservable<T extends Payload> = Observable<ApiResponseDto<T>>;
