
export interface AbstractSaveProjectRequest {

  name: string;
  description: string;
  location: string;

}

