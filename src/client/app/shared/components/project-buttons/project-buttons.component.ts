import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StateService } from '../../services/state.service';
import { SessionService } from '../../services/session.service';
import { ViewModeService } from '../../services/view-mode.service';
import { RedirectService } from '../../services/redirect.service';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'cd-project-buttons',
  templateUrl: 'project-buttons.component.html',
})
export class ProjectButtonsComponent implements OnInit {
  public showConfirmDialog = false;
  public confirmDialogLeftPosition: number;

  @Input() disabled: boolean;
  @Input() showSubmit: boolean;
  @Input() showShare: boolean;
  @Input() showDelete: boolean;
  @Input() showNext: boolean;
  @Input() showCancel: boolean;
  @Input() showSubmitToVote: boolean;
  @Input() showVote: boolean;
  @Input() labels: any;
  @Input() isLocked: boolean;

  @Output() share: EventEmitter<null> = new EventEmitter<null>();
  @Output() remove: EventEmitter<null> = new EventEmitter<null>();
  @Output() next: EventEmitter<null> = new EventEmitter<null>();
  @Output() cancel: EventEmitter<null> = new EventEmitter<null>();
  @Output() submit: EventEmitter<null> = new EventEmitter<null>();
  @Output() vote: EventEmitter<null> = new EventEmitter<null>();
  @Output() submitToVote: EventEmitter<null> = new EventEmitter<null>();

  constructor(
    private viewModeService: ViewModeService,
    private redirectService: RedirectService,
    private sessionService: SessionService,
    private stateService: StateService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.confirmDialogLeftPosition = (window.innerWidth - this.confirmDialogLeftPosition) / 2;
  }

  onShare() {
    if (this.sessionService.isAuthorized()) {
      this.share.emit();
    } else {
      this.redirectService.setRedirectUrl(this.router.url);
      this.viewModeService.showViewModeDialog();
    }
  }

  onRemove() {
    this.remove.emit();
  }

  onNext() {
    this.next.emit();
  }

  onCancel() {
    this.cancel.emit();
  }

  onVote() {
    this.vote.emit();
  }

  onSubmit() {
    this.submit.emit();
  }

  onSubmitToVote() {
    this.stateService.hideOverlayDialog();
    this.showConfirmDialog = false;
    this.submitToVote.emit();
  }

  onShowConfirmDialog() {
    this.stateService.showOverlayDialog();
    this.showConfirmDialog = true;
  }

  onHideConfirmDialog() {
    this.stateService.hideOverlayDialog();
    this.showConfirmDialog = false;
  }
}

