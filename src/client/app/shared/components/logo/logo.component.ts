import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'cd-logo',
  templateUrl: 'logo.component.html',
  styleUrls: ['logo.component.css'],
})
export class LogoComponent {
}
