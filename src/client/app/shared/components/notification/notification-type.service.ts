export enum NotificationType {
  NOTIFY = '',
  WARNING = 'fa-exclamation-triangle',
  ERROR = 'fa-exclamation',
  IMPORTANT = 'fa-exclamation important-notification',
  DONE = 'fa-check',
  FAIL = 'fa-close'
}
