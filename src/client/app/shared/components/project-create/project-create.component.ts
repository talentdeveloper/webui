import { OnDestroy, OnInit } from '@angular/core';
import { LocationService } from '../../../home/components/side-menu/location/location.service';
import { NotificationService } from '../notification/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { I18N_PROJECT_ACTIONS, I18N_PROJECT_TYPES } from '../../i18n/elements';
import { TranslateService } from '@ngx-translate/core';
import { AbstractProject } from '../../api/common/AbstractProject';
import { AbstractProjectService } from '../../services/abstract-project.service';
import { LoadingIndicatorService } from '../../services/loading-indicator.service';
import { AbstractSaveProjectRequest } from '../../api/common/AbstractSaveProjectRequest';
import { NavigationService } from '../../map/navigation.service';
import { Location } from '../../api/models/location.models';
import { StateService } from '../../services/state.service';
import { ProjectTypeService } from '../../../home/components/side-menu/project/project-type.service';
import { ProjectType } from '../../api/models/project-type.model';
import { BaseLockComponent } from '../../utils/baseLockComponent';

export abstract class AbstractProjectCreateComponent<T extends AbstractProject> extends BaseLockComponent implements OnInit, OnDestroy {

  public badRequest: string;
  public projectType: ProjectType;

  protected visible = false;
  protected projectTypeString = 'project';
  protected location: Location;
  protected project: T;

  private clearAfterDestroy = true;

  protected constructor(protected router: Router,
                        protected route: ActivatedRoute,
                        protected projectService: AbstractProjectService<T>,
                        protected loadingIndicatorService: LoadingIndicatorService,
                        protected locationService: LocationService,
                        protected notificationService: NotificationService,
                        protected navigationService: NavigationService,
                        protected stateService: StateService,
                        protected projectTypeService: ProjectTypeService,
                        protected translate: TranslateService) {
    super();
  }

  public getProjectModel(): T {
    return this.project;
  }

  public cancel() {
    if (this.location.locked === false) {
      this.lockComponent();
      this.locationService.removeLocation(this.location.id)
        .finally(() => this.unlockComponent())
        .subscribe(() => {
          this.clearAfterDestroy = false;
          this.visible = false;
          this.navigationService.removeLocationMarker(this.location.id);
          this.router.navigate(['../../../'], {relativeTo: this.route});
        });
    } else {
      this.onHide();
    }
    this.translate.get(I18N_PROJECT_TYPES[this.projectTypeString]).subscribe((projectType: string) => {
      this.translate.get(I18N_PROJECT_ACTIONS.cancel)
        .subscribe((cancelStr: string) => {
          this.notificationService.show(cancelStr.replace('${projectType}', projectType));
        });
    });
  }

  public ngOnInit() {
    this.onInit();
    this.navigationService.forbidLocationSelection();

    this.stateService.hideLocationModal();
    this.visible = true;

    this.route.parent.data.subscribe((data: any) => {
      this.location = data.location;
    });

    this.project = this.createProject();

    this.beforeCreateProject();
  }

  public ngOnDestroy() {
    this.navigationService.allowLocationSelection();

    if (this.clearAfterDestroy) {
      this.cancel();
    }
    this.navigationService.enableCameraRotation(true);
    this.navigationService.enableCameraZoom(true);
  }

  public submit(saveRequest: AbstractSaveProjectRequest, cb?: () => void) {
    this.lockComponent();
    this.badRequest = null;
    this.projectService.addProject(saveRequest)
      .finally(() => this.unlockComponent())
      .subscribe((project: T) => {
      this.clearAfterDestroy = false;
      this.router.navigate([project.id], {relativeTo: this.route});
      if (this.afterSaveResponse) {
        this.afterSaveResponse();
      }
    }, (error) => {
      this.badRequest = error.extra._body.service.error_message;
      this.onSubmitError();
    });
  }

  public onShow() {
    this.visible = true;
  }

  public onHide() {
    this.visible = false;
    this.stateService.showLocationModal();
    this.locationService.getLocation(this.location.id)
      .subscribe((location: Location) => {
        this.navigationService.addLocationMarker(location);
      });
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  public isVisible(): boolean {
    return this.visible;
  }

  protected onSubmitError() {
  }

  protected onInit() {
  }

  protected afterSaveResponse?(): void;

  protected abstract createProject(): T;

  protected beforeCreateProject() {
    this.projectTypeService.getProjectTypeByName(this.projectTypeString)
      .subscribe((projectType: ProjectType) => {
        this.projectType = projectType;
        this.stateService.selectLocation(this.projectType.name);
      });
  }
}
