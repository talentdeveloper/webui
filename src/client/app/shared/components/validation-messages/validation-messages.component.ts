import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  moduleId: module.id,
  selector: 'cd-validation-messages',
  templateUrl: 'validation-messages.component.html',
  styleUrls: ['validation-messages.component.css'],
})
export class ValidationMessagesComponent implements OnInit {
  @Input() control: AbstractControl;
  @Input() validationsObject: any;
  @Input() showRequired = true;
  public errorKeys: Array<any>;

  ngOnInit() {
    this.errorKeys = Object.keys(this.validationsObject);
  }

  controlHasError(errorKey: string): boolean {
    if (this.control.errors) {
      if (this.showRequired) {
        return this.control.hasError(errorKey);
      } else {
        return this.control.hasError(errorKey) && !this.controlHasRequiredError();
      }
    }
    return false;
  }

  controlHasRequiredError() {
    return this.control.hasError('required') || this.control.hasError('beneficialRequired');
  }
}
