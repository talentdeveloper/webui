import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MenuPanelComponent } from './menu-panel.component';
import { SubmenuPanelComponent } from './submenu/submenu-panel.component';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule],
  exports: [MenuPanelComponent, RouterModule],
  declarations: [MenuPanelComponent, SubmenuPanelComponent]
})
export class MenuPanelModule {
}
