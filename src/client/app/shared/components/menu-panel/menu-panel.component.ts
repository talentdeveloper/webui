import { Component, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { BasePanelMenuItem } from './base-panel-menu.service';
import { MenuItem } from 'primeng/primeng';
import { IMAGES } from '../../constants/images';


@Component({
  moduleId: module.id,
  selector: 'cd-menu-panel',
  templateUrl: 'menu-panel.component.html',
  animations: [
    trigger('rootItem', [
      state('hidden', style({
        height: '0px'
      })),
      state('visible', style({
        height: '*'
      })),
      transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class MenuPanelComponent extends BasePanelMenuItem {

  @Input() model: MenuItem[];

  @Input() style: any;

  @Input() styleClass: string;

  @Input() multiple = false;

  @Input() images = false;

  @Input() childImages = false;

  public animating: boolean;

  defaultImagePath: string = IMAGES.DEFAULT_IMAGE_URL;

  collapseAll() {
    for (const item of this.model) {
      if (item.expanded) {
        item.expanded = false;
      }
    }
  }

  handleClick(event: Event, item: any) {
    if (!this.multiple) {
      for (const modelItem of this.model) {
        if (item !== modelItem && modelItem.expanded) {
          modelItem.expanded = false;
        }
      }
    }

    this.animating = true;
    super.handleClick(event, item);
  }

  onToggleDone() {
    this.animating = false;
  }

}
