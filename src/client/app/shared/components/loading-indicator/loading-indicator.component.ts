import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'cd-loading-indicator',
  templateUrl: 'loading-indicator.component.html',
  styleUrls: ['loading-indicator.component.css']
})
export class LoadingIndicatorComponent {

}
