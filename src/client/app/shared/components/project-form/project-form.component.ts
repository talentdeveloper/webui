import { EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { LocationService } from '../../../home/components/side-menu/location/location.service';
import { AbstractProject } from '../../api/common/AbstractProject';
import { LoadingIndicatorService } from '../../services/loading-indicator.service';
import { I18N_ERROR_MESSAGES } from '../../i18n/elements';
import { AbstractProjectService } from '../../services/abstract-project.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../notification/notification.service';
import { PromoteThemService } from '../../../home/components/side-menu/promote/promote-them/promote-them.service';
import { SessionService } from '../../services/session.service';
import { ROUTES } from '../../constants/routes';
import { SizeService } from '../../services/size.service';
import { Subscription } from 'rxjs/Subscription';
import { AbstractSaveProjectRequest } from '../../api/common/AbstractSaveProjectRequest';
import { ConditionService } from '../../services/condition.service';
import { NavigationService } from '../../map/navigation.service';
import { BaseLockComponent } from '../../utils/baseLockComponent';

export abstract class AbstractProjectFormComponent<T extends AbstractProject, K extends AbstractSaveProjectRequest> extends BaseLockComponent implements OnInit, OnDestroy {
  public showConfirmDialog = false;
  public fullName: string;
  public ROUTES = ROUTES;
  public isIE11: boolean;
  public toggle = true;
  public dialogClass: string;
  public dialogPositionTop = 60;
  public dialogPositionLeft = 180;

  @Input()
  set project(project: T) {
    this._project = project;
    this.initForm();
  }

  get project() {
    return this._project;
  }

  @Input() badRequest: string;
  @Input() header: string;
  @Input() subHeader: string;
  @Input() editMode = false;
  @Input() createMode = false;
  @Output() show: EventEmitter<null> = new EventEmitter<null>();
  @Output() hide: EventEmitter<null> = new EventEmitter<null>();
  @Output() submit: EventEmitter<K> = new EventEmitter<K>();
  @Output() next: EventEmitter<null> = new EventEmitter<null>();
  @Output() remove: EventEmitter<null> = new EventEmitter<null>();
  @Output() cancel: EventEmitter<null> = new EventEmitter<null>();
  @Output() viewMode: EventEmitter<boolean> = new EventEmitter<boolean>();
  private visible: boolean;
  private widthSubscription: Subscription;
  private _project: T;

  protected constructor(
    protected promoteThemService: PromoteThemService,
    protected projectService: AbstractProjectService<T>,
    protected locationService: LocationService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected sessionService: SessionService,
    protected sizeService: SizeService,
    protected conditionService: ConditionService,
    protected navigationService: NavigationService,
  ) {
    super();
  }

  abstract initForm(): void;

  abstract buildSaveRequest(): K;

  public ngOnInit(): void {
    this.navigationService.freezeCamera();

    this.widthSubscription = this.sizeService.getMenuWidth()
      .subscribe((menuWidth: number) => {
        if (menuWidth) {
          this.dialogPositionLeft = Math.round(menuWidth) + 10;
        }
      });

    this.isIE11 = !!(window as any).MSInputMethodContext && !!(document as any).documentMode;
    if (this.sessionService.getSecurityPrincipal()) {
      this.fullName = this.sessionService.getSecurityPrincipal().getPrincipalFullName();
    }
    this.initForm();
    if (this.onInit) {
      this.onInit();
    }
  }

  public ngOnDestroy(): void {
    this.navigationService.unfreezeCamera();

    if (this.widthSubscription) {
      this.widthSubscription.unsubscribe();
    }

    if (this.onDestroy) {
      this.onDestroy();
    }
  }

  public setEditMode(): void {
    this.viewMode.emit(false);
  }

  public setViewMode(): void {
    this.initForm();
    this.viewMode.emit(true);
  }

  public onCancel() {
    if (this.editMode && this.createMode) {
      this.cancel.emit();
    } else if (this.editMode) {
      return this.setViewMode();
    }
  }

  public share(type: string) {
    this.promoteThemService.show(this.project, type);
  }

  public submitToVote() {
    this.lockComponent();
    this.projectService.submitToVote(this.project.id)
      .finally(() => this.unlockComponent())
      .subscribe((project: T) => {
          this.showConfirmDialog = false;
          this.project = project;
          this.translate.get(I18N_ERROR_MESSAGES.submittedProject)
            .subscribe(res => {
              this.notificationService.show(res);
            });
        },
        error => {
          if (error.errorCode === 4) {
            this.translate.get(I18N_ERROR_MESSAGES.canNotSubmitProject)
              .subscribe(res => {
                this.notificationService.show(res);
              });
          }
        });
  }

  public vote() {
    this.lockComponent();
    this.projectService.vote(this.project.id)
      .finally(() => this.unlockComponent())
      .subscribe((project: T) => {
        this.project = project;
      });
  }

  public isUserCreator(): boolean {
    if (this.sessionService.isAuthorized()) {
      return this.sessionService.getSecurityPrincipal().isItMy(this.project);
    } else {
      return false;
    }
  }

  public canBeModified(): boolean {
    return this.isUserCreator() && (!this.project.underVoting && !this.project.wonVoting);
  }

  public canProjectBeVoted(): boolean {
    return this.project.canVote && this.project.underVoting;
  }

  public canProjectBeSubmittedToVote(): boolean {
    return this.conditionService.canAbstractProjectBeSubmittedToVote(this.project) && !this.editMode;
  }

  public onShow(): void {
    this.visible = true;
    this.show.emit(null);
  }

  public onHide(): void {
    this.visible = false;
    this.hide.emit(null);
  }

  public onSubmit(): void {
    const saveRequest = this.buildSaveRequest();
    this.submit.emit(saveRequest);
  }

  public nextProject() {
    this.next.emit();
  }

  public removeProject() {
    this.remove.emit();
  }

  public isVisible(): boolean {
    return this.visible;
  }

  public getStyleClass(): string {
    return `${this.dialogClass}`;
  }

  protected onInit?(): void;

  protected onDestroy?(): void;
}
