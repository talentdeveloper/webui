import { OnDestroy, OnInit } from '@angular/core';
import { LocationService } from '../../../home/components/side-menu/location/location.service';
import { AbstractProject } from '../../api/common/AbstractProject';
import { LoadingIndicatorService } from '../../services/loading-indicator.service';
import { AbstractProjectService } from '../../services/abstract-project.service';
import { ROUTES } from '../../constants/routes';
import { ActivatedRoute, Router } from '@angular/router';
import { PromoteThemService } from '../../../home/components/side-menu/promote/promote-them/promote-them.service';
import { Subscription } from 'rxjs/Subscription';
import { AbstractSaveProjectRequest } from '../../api/common/AbstractSaveProjectRequest';
import { NavigationService } from '../../map/navigation.service';
import { StateService } from '../../services/state.service';
import { SessionService } from '../../services/session.service';
import { Location } from '../../api/models/location.models';
import { ProjectMinimalData } from '../../api/models/projectminimaldata';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../notification/notification.service';
import { I18N_ERROR_MESSAGES } from '../../i18n/elements';
import { PROJECT_TYPES } from '../../constants/projectTypes';
import { ProjectType } from '../../api/models/project-type.model';
import { ProjectTypeService } from '../../../home/components/side-menu/project/project-type.service';
import { BaseLockComponent } from '../../utils/baseLockComponent';

export abstract class AbstractProjectViewComponent<T extends AbstractProject> extends BaseLockComponent implements OnInit, OnDestroy {
  public project: T;

  public badRequest: string;

  public projectType: ProjectType;

  public editMode: boolean;
  private location: Location;
  private _visible = true;

  private _removeInProgress = false;

  private _subscription: Subscription;

  protected constructor(
    protected projectService: AbstractProjectService<T>,
    protected locationService: LocationService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected promoteThemService: PromoteThemService,
    protected stateService: StateService,
    protected navigationService: NavigationService,
    protected sessionService: SessionService,
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected projectTypeService: ProjectTypeService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.onInit();
    this.navigationService.forbidLocationSelection();

    this.stateService.hideLocationModal();
    this.route.data.subscribe((data: any) => {
      this.setProject(data.project);
    });

    this.route.parent.data.subscribe((data: any) => {
      this.location = data.location;
    });

  }

  public setProject(project: T): void {
    this.project = project;
    this.afterSetProject();
  }

  public canBeModified(): boolean {
    return this.isUserCreator() && !this.project.underVoting;
  }

  public getProjectModel(): T {
    return this.project;
  }

  public cancel() {
    this.editMode = false;
    this.projectService.getProject(this.project.id)
      .subscribe((project: T) => {
        this.project = project;
      });
  }

  public ngOnDestroy(): void {
    this.navigationService.allowLocationSelection();

    if (this._subscription) {
      this._subscription.unsubscribe();
    }
    this.promoteThemService.hide();
  }

  public onHide() {
    this._visible = false;
    this.promoteThemService.hide();
    this.stateService.showLocationModal();
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  public onShow() {
    this._visible = true;
  }

  public isVisible() {
    return this._visible;
  }

  public submit(saveRequest: AbstractSaveProjectRequest, cb?: () => void, errorCb?: () => void) {
    this.lockComponent();
    this.projectService.updateProject(this.getProjectModel().id, saveRequest)
      .finally(() => this.unlockComponent())
      .subscribe((project: T) => {
      this.project = project;
      this.editMode = false;
      if (cb) {
        cb();
      }
    }, (error) => {
      this.badRequest = error.extra._body.service.error_message;
      this.onSubmitError();
    });
  }

  public removeProject(type: string, cb?: () => void) {
    const model = this.getProjectModel();
    if (this.sessionService.getSecurityPrincipal().isItMy(model) && !this._removeInProgress) {
      this._removeInProgress = true;
      this.lockComponent();
      this.projectService.removeProject(model.id)
        .finally(() => this.unlockComponent())
        .subscribe(() => {
          this.locationService.getLocation(this.location.id)
            .subscribe((location: Location) => {
              if (location.locked === false) {
                this.locationService.removeLocation(this.location.id).subscribe(() => {
                  this.navigationService.removeLocationMarker(this.location.id);
                  this.router.navigate(['../../../../'], {relativeTo: this.route});
                });
              } else {
                this.router.navigate(['../../'], {relativeTo: this.route})
                  .then(() => this.stateService.showLocationModal());
              }
            });

            if (cb) {
              cb();
            }
          },
          () => {
            this._removeInProgress = false;
          },
          () => {
            this._removeInProgress = false;
          });
    }
  }

  public isUserCreator(): boolean {
    return this.sessionService.getSecurityPrincipal().isItMy(this.getProjectModel());
  }

  public nextProject(type: string) {
    // Checking fot type of project and identifying entity collection
    let projects: ProjectMinimalData[];
    switch (type) {
      case PROJECT_TYPES.EVENT:
        projects = this.location.events;
        break;
      case PROJECT_TYPES.PUBLIC_PROJECT:
        projects = this.location.projects;
        break;
      case PROJECT_TYPES.IDEA:
        projects = this.location.messages;
        break;
      default:
        break;
    }

    if (projects.length === 1) {
      // Notification about single project in location
      switch (type) {
        case PROJECT_TYPES.EVENT:
          this.translate.get(I18N_ERROR_MESSAGES.singleEvent)
            .subscribe(res => {
              this.notificationService.show(res);
            });
          break;
        case PROJECT_TYPES.PUBLIC_PROJECT:
          this.translate.get(I18N_ERROR_MESSAGES.singleProject)
            .subscribe(res => {
              this.notificationService.show(res);
            });
          break;
        case PROJECT_TYPES.IDEA:
          this.translate.get(I18N_ERROR_MESSAGES.singleMessage)
            .subscribe(res => {
              this.notificationService.show(res);
            });
          break;
        default:
          break;
      }
    } else {
      projects.forEach((project: ProjectMinimalData, index) => {
        if (project.id === this.project.id) {
          const nextProject: ProjectMinimalData = projects.length - 1 === index ? projects[0] : projects[index + 1];
          switch (type) {
            case PROJECT_TYPES.EVENT:
              this.router.navigate(['../../', ROUTES.event, nextProject.id], {relativeTo: this.route});
              break;
            case PROJECT_TYPES.PUBLIC_PROJECT:
              this.router.navigate(['../../', ROUTES.project, nextProject.id], {relativeTo: this.route});
              break;
            case PROJECT_TYPES.IDEA:
              this.router.navigate(['../../', ROUTES.inspire, nextProject.id], {relativeTo: this.route});
              break;
            default:
              break;
          }
        }
      });
    }
  }

  public setViewMode(viewMode: boolean) {
    this.editMode = !viewMode;
  }

  protected onSubmitError() {}

  protected onInit() {}

  protected afterSetProject() {
    this.projectTypeService.getProjectTypeByName(this.project.location.type)
      .subscribe((projectType: ProjectType) => {
        this.projectType = projectType;
      });
  }
}
