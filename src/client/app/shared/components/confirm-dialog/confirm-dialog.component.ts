import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { ContextHolderService } from '../../services/context-holder.service';

@Component({
  moduleId: module.id,
  selector: 'cd-confirm-dialog',
  templateUrl: 'confirm-dialog.component.html',
  styleUrls: ['confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements AfterViewInit {
  public overlay: any;
  @Input() showDialog: boolean;
  @Input() confirmationText: string;
  @Input() confirmButtonText: string;
  @Input() cancelButtonText: string;
  @Input() headerText: string;
  @Input() width: number;
  @Input() positionLeft: number;
  @Input() positionTop: number;
  @Input() isLocked: boolean;
  @Output() confirm: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  constructor(private contextHolderService: ContextHolderService) {}

  public ngAfterViewInit() {
    // Hack
    setTimeout(() => {
      this.overlay = this.contextHolderService.getModalOverlayElement().nativeElement;
    }, 0);
  }

  public onConfirm() {
    this.confirm.emit();
  }

  public onCancel() {
    this.cancel.emit();
  }

}
