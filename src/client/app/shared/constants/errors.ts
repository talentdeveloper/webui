export const ERROR_MESSAGES = {
  INCORRECT_CREDENTIALS: 'Incorrect credentials',
  NAV_ERROR: 'Error occurred when navigating',
  WRONG_PASSWORD: 'Wrong password',
};
