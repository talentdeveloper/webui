const DELETE = 'Delete';
const NEXT = 'Next';
const APPLY = 'Apply';
const SAVE = 'Save';
const EDIT = 'Edit';
const UPDATE = 'Update';
const SUBMIT = 'Submit';
const TELL_MORE = 'Please tell us more about it';
const MORE_DETAILS = 'More details about it';
const SHOW_ME_MORE = 'Show me more!';
const SHARE = 'Share';
const CANCEL = 'Cancel';
const SUBMIT_TO_VOTE = 'Submit to vote';
const VOTE = 'Vote';
const CONFIRM_DIALOG = {
  CONFIRMATION_TEXT: 'After you submit your business project to vote it cannot be either changed or deleted. Are you sure you want to submit this business project to vote?',
  CANCEL_BUTTON_TEXT: CANCEL,
};

export const CONSTANTS = {
  BREAKPOINT_WIDTH: 980,
  LOGIN: {
    SIGN_IN: 'Sign in',
    SIGN_UP: 'Sign up',
    FROM_RESET: 'Now you can login with your new password.'
  },
  FORGOT: {
    SUBMIT: SUBMIT,
    EMAIL: 'Email',
    FORGOT_PASSWORD: 'Forgot password',
    FORGOT_INSTRUCTIONS: 'To reset your password please enter your email address. An email with further instructions will be sent.',
    FORGOT_EMAIL_SENT: 'An email with further instructions has been sent'
  },
  RESET: {
    SUBMIT: SUBMIT,
    RESET_ERROR: 'This link to reset your password has expired or has already been used. Go back to Login Page and request your password reset again.',
  },
  LOCATION: {
    HEADER: 'Location details',
    DELETE: DELETE,
  },
  PROJECT: {
    IMPACT: {
      HEADER: 'Project impact',
    },
    CONFIRM_DIALOG: {
      CONFIRMATION_TEXT: 'After you submit your project to vote it cannot be either changed or deleted. Are you sure you want to submit this project to vote?',
      CANCEL_BUTTON_TEXT: CANCEL,
    },
    CREATE: {
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    VIEW: {
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    HEADER: 'Public project details',
    SUBMIT_TO_VOTE: SUBMIT_TO_VOTE,
    VOTE: VOTE,
    CANCEL: CANCEL,
    NEXT: NEXT,
    DELETE: DELETE,
    APPLY: APPLY,
    EDIT: EDIT,
    SHARE: SHARE,
    SAVE: SAVE,
  },
  EVENT: {
    CREATE: {
      HEADER: 'New Event',
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    VIEW: {
      HEADER: 'Event',
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    CONFIRM_DIALOG: {
      CONFIRMATION_TEXT: 'After you submit your event to vote it cannot be either changed or deleted. Are you sure you want to submit this event to vote?',
      CANCEL_BUTTON_TEXT: CANCEL,
    },
    SUBMIT_TO_VOTE: SUBMIT_TO_VOTE,
    VOTE: VOTE,
    CANCEL: CANCEL,
    NEXT: NEXT,
    DELETE: DELETE,
    APPLY: APPLY,
    EDIT: EDIT,
    SHARE: SHARE,
    SAVE: SAVE,
  },
  INSPIRE: {
    CREATE: {
      HEADER: 'New Idea',
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    VIEW: {
      HEADER: 'Idea',
      TELL_MORE: TELL_MORE,
      MORE_DETAILS: MORE_DETAILS
    },
    CONFIRM_DIALOG: {
      CONFIRMATION_TEXT: 'After you submit your idea to vote it cannot be either changed or deleted. Are you sure you want to submit this idea to vote?',
      CANCEL_BUTTON_TEXT: CANCEL,
    },
    SUBMIT_TO_VOTE: SUBMIT_TO_VOTE,
    VOTE: VOTE,
    INSPIRE: 'Inspire',
    NEXT: NEXT,
    CANCEL: CANCEL,
    DELETE: DELETE,
    APPLY: APPLY,
    UPDATE: UPDATE,
    EDIT: EDIT,
    SHARE: SHARE,
    SAVE: SAVE,
  },
  PROMOTE: {
    SHARE_ALL: 'Share on all your accounts to promote the project',
  },
  MENU: {
    SUGGEST_WHERE: {
      LABEL: 'Suggest where',
    },
    FIND_WHERE: {
      LABEL: 'Find where',
    },
    INSPIRE: {
      LABEL: 'Inspire',
      AS_A_POET: {
        LABEL: 'As a poet',
      },
      AS_A_STORY_TELLER: {
        LABEL: 'As a story teller',
      },
      AS_A_HISTORIAN: {
        LABEL: 'As a historian',
      },
    },
    GET_INSPIRED: {
      LABEL: 'Get Inspired',
      BY_POETRY: {
        LABEL: 'By poetry',
      },
      BY_STORY: {
        LABEL: 'By story',
      },
      BY_THE_HISTORY: {
        LABEL: 'By the history',
      },
    },
    BUILD_YOUR_DREAM: {
      LABEL: 'Build your dream',
      AS_A_MAYOR: {
        LABEL: 'As a mayor',
      },
      AS_AN_URBANIST: {
        LABEL: 'As an urbanist',
      },
      AS_AN_ARCHITECT: {
        LABEL: 'As an architect',
      },
      AS_A_PROJECT_PROMOTER: {
        LABEL: 'As an entrepreneur',
      },
      AS_AN_EVENT_MANAGER: {
        LABEL: 'As as event manager',
      },
    },
    SHARE_YOUR_DREAMS: {
      LABEL: 'Share your dreams',
      MEET_PARTNERS: {
        LABEL: 'Meet partners',
      },
      CREATE_YOUR_GROUPS: {
        LABEL: 'Create your groups',
      },
      SUBMIT_TO_MODIFICATION: {
        LABEL: 'Submit to modification',
      },
      SUBMIT_TO_VOTE: {
        LABEL: 'Submit to vote',
      }
    },
    VISIT_SELECTED_DREAMS: {
      LABEL: 'Visit selected dreams',
      IMPROVE_THEM: {
        LABEL: 'Improve them',
      },
      PROMOTE_THEM: {
        LABEL: 'Promote them',
      },
    },
    MAKE_THEM_REAL: {
      LABEL: 'Make them real',
      VOTE: {
        LABEL: 'Vote!',
      },
      FINANCE: {
        LABEL: 'Finance!',
      },
      REALISE: {
        LABEL: 'REALISE!',
      },
    },
    ABOUT_THE_GAME: {
      LABEL: 'About the game',
    },
    GAME_RULES: {
      LABEL: 'Game rules',
    },
  },
  PROFILE_EDIT: {
    HEADER: 'Edit info',
    UPDATE: UPDATE,
    NOTIFICATIONS: {
      HEADER: 'Notification settings',
      ON_VOTE: 'Votes for your project',
      GET_NOTIFIED: 'Get notified by email when someone...',
      UPDATE: UPDATE,
    },
    CHANGE_PASSWORD: {
      HEADER: 'Change password',
    },
    CREATE_PASSWORD: {
      HEADER: 'Create password',
    }
  },
  PROFILE: {
    SUBMIT_TO_VOTE: SUBMIT_TO_VOTE,
    CONFIRM_DIALOG: CONFIRM_DIALOG,
    PUBLIC_LABEL: 'Public projects',
    PRIVATE_LABEL: 'Private projects',
    HEADER: 'Welcome to Codemos'
  },
  VOTING: {
    SHOW_ME_MORE: SHOW_ME_MORE,
  },
};

export const ATTRIBUTIONS = {
  AUTHORS: {
    FREEPIK: {
      url: 'http://www.freepik.com',
      name: 'Freepik'
    }
  },
  SITES: {
    FLATICON: {
      url: 'https://www.flaticon.com/',
      displayUrl: 'www.flaticon.com',
      name: 'Flaticon'
    }
  },
  LICENSES: {
    CC3: {
      url: 'http://creativecommons.org/licenses/by/3.0/',
      fullName: 'Creative Commons BY 3.0',
      shortName: 'CC 3.0 BY'
    }
  }
};
