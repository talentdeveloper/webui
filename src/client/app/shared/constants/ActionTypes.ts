export const ACTION_TYPES = {
  UPDATE: {
    type: 'UPDATE',
    headerText: 'Update Event',
    buttonText: 'Update Event Button'
  },
  CREATE: {
    type: 'CREATE',
    headerText: 'Create Event',
    buttonText: 'Create Event Button'
  }
};
