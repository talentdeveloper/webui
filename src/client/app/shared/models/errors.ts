import { ServiceSection } from '../api/models/servicesection';

export class ApiError extends Error {
  private _validationErrors: any;
  private _extra: any;
  private _status: number;
  private _verbose_error_message: string;
  private _error_code: number;

  constructor (service: ServiceSection, extra?: any) {
    super(['API error: [', service.errorCode, '] ', service.errorMessage || extra && extra['statusText']].join(''));
    this.validationErrors = service.validationErrors || null;
    this.extra = extra || null;
    this.verboseErrorMessage = service.verboseErrorMessage || null;
    this.status = extra && extra['status'] || service.errorCode;
    this.errorCode = service.errorCode;
  }

  set validationErrors(value: any) {
    this._validationErrors = value;
  }

  get validationErrors(): any {
    return this._validationErrors;
  }

  set extra(value: any) {
    this._extra = value;
  }

  get extra(): any {
    return this._extra;
  }

  set verboseErrorMessage(value: string) {
    this._verbose_error_message = value;
  }

  get verboseErrorMessage(): string {
    return this._verbose_error_message;
  }

  set errorCode(value: number) {
    this._error_code = value;
  }

  get errorCode(): number {
    return this._error_code;
  }

  set status(value: number) {
    this._status = value;
  }

  get status(): number {
    return this._status;
  }
}
