import { User } from '../api/models/user';
import { CreatorAware } from '../api/common/CreatorAware';

export interface SecurityPrincipal {
  getPrincipalId(): string;

  getPrincipalScore(): number;

  addPrincipalScore(amount: number): void;

  getPrincipalFullName(): string;

  getPrincipalFirstName(): string;

  getPrincipalLastName(): string;

  getPrincipalLanguage(): string;

  getPrincipalGender(): string;
  
  getPrincipalBirthDate(): string;

  getPrincipalTrophiesCount(): number;

  getPrincipalTitle(): string;

  getPrincipalCity(): string;

  getPrincipalAddress(): string;  

  getPrincipalHasPasswordField(): boolean;

  getPrincipalUseNotificationsField(): boolean;

  getPrincipalJSON(): any;

  isItMy(obj: CreatorAware): boolean;
}

export class UserPrincipal implements SecurityPrincipal {

  public user: User;

  constructor(securityPrincipal: any) {
    this.user = securityPrincipal ? new User(securityPrincipal.user) : null;
  }

  public getPrincipalId(): string {
    return this.user.id;
  }

  public getPrincipalScore(): number {
    return this.user.score;
  }

  public addPrincipalScore(amount: number) {
    this.user.score += amount;
  }

  public getPrincipalFullName(): string {
    return `${this.user.firstName} ${this.user.lastName}`;
  }

  public getPrincipalFirstName(): string {
    return this.user.firstName;
  }

  public getPrincipalLastName(): string {
    return this.user.lastName;
  }

  public getPrincipalLanguage(): string {
    return this.user.language;
  }

  public getPrincipalGender(): string {
    return this.user.gender;
  }

  public getPrincipalBirthDate(): string {
    return this.user.birthDate;
  }

  public getPrincipalTrophiesCount(): number {
    return this.user.trophies;
  }

  public getPrincipalTitle(): string {
    return this.user.title;
  }

  public getPrincipalCity(): string {
    return this.user.city;
  }

  public getPrincipalAddress(): string {
    return this.user.address;
  }

  public getPrincipalHasPasswordField(): boolean {
    return this.user.hasPassword;
  }

  public getPrincipalUseNotificationsField(): boolean {
    return this.user.useNotifications;
  }

  public getPrincipalJSON(): any {
    return this.user.toJSON();
  }

  public isItMy(obj: CreatorAware): boolean {
    if (this.user && obj && obj.creator) {
      return obj.creator.id === this.getPrincipalId();
    }
    // TODO Review line below
    return !!this.user;
  }
}
