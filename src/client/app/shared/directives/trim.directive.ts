import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[trim]'
})
export class TrimDirective {
  element: ElementRef;
  renderer: Renderer2;

  constructor(
    _element: ElementRef,
    _renderer: Renderer2
  ) {
    this.element = _element;
    this.renderer = _renderer;
  }

  @HostListener('change') onChange() {
    const value = this.element.nativeElement.value.trim();
    this.renderer.setProperty(this.element.nativeElement, 'value', value);
    const event: Event = document.createEvent('Event');
    event.initEvent('input', true, true);
    Object.defineProperty(event, 'target', {value: this.element.nativeElement, enumerable: true});
    this.element.nativeElement.dispatchEvent(event);
  }
}
