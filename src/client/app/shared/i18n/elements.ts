import { i18n } from './utils';
import { Dictionary } from '../types';

export const I18N_SIDE_MENU = {
  suggest: i18n('Suggest where'),
  find: i18n('Find where'),
  inspire: i18n('Inspire'),
  asPoetry: i18n('As a poetry'),
  asStoryTeller: i18n('As a story teller'),
  asHistorian: i18n('As a historian'),
  getInspired: i18n('Get Inspired'),
  byPoetry: i18n('By poetry'),
  byStory: i18n('By story'),
  byHistory: i18n('By the history'),
  buildDream: i18n('Build your dream'),
  asMayor: i18n('As a mayor'),
  asUrbanist: i18n('As an urbanist'),
  asArchitect: i18n('As an architect'),
  asEntrepreneur: i18n('As an entrepreneur'),
  asEventManager: i18n('As an event manager'),
  shareDreams: i18n('Share your dreams'),
  meetPartners: i18n('Meet partners'),
  createGroups: i18n('Create your groups'),
  submitModification: i18n('Submit to modification'),
  submitToVote: i18n('Submit to vote'),
  visitSelectedDreams: i18n('Visit selected dreams'),
  improveThem: i18n('Improve them'),
  promoteThem: i18n('Promote them'),
  makeThemReal: i18n('Make them real'),
  vote: i18n('Vote!'),
  finance: i18n('Finance!'),
  realise: i18n('REALISE!'),
  aboutTheGame: i18n('About the game'),
  gameRules: i18n('Game rules'),
  navHints: i18n('Navigation hints'),
};

export const I18N_COMMON_BUTTONS = {
};

export const I18N_GENDERS: Dictionary<string> = {
  f: i18n('Female'),
  m: i18n('Male'),
};

export const I18N_PRIVATE_CATEGORIES: Dictionary<string> = {
  'Laundry': 'Laundry',
  'Bicycle Store': 'Bicycle Store',
  'Travel Agency': 'Travel Agency',
  'Hardware Store': 'Hardware Store',
  'Convenience Store': 'Convenience Store',
  'Beauty Salon': 'Beauty Salon',
  'Electrician': 'Electrician',
  'Car Wash': 'Car Wash',
  'Electronics Store': 'Electronics Store',
  'Bar': 'Bar',
  'Movie Rental': 'Movie Rental',
  'Bakery': 'Bakery',
  'Car Repair': 'Car Repair',
  'Real Estate Agency': 'Real Estate Agency',
  'Home Goods Store': 'Home Goods Store',
  'Department Store': 'Department Store',
  'Store': 'Store',
  'Atm': 'Atm',
  'Bank': 'Bank',
  'Cafe': 'Cafe',
  'Shopping Mall': 'Shopping Mall',
  'Car Dealer': 'Car Dealer',
  'Meal Takeaway': 'Meal Takeaway',
  'Insurance Agency': 'Insurance Agency',
  'Restaurant': 'Restaurant',
  'Book Store': 'Book Store',
  'Car Rental': 'Car Rental',
  'Furniture Store': 'Furniture Store',
  'Locksmith': 'Locksmith',
  'Veterinary Care': 'Veterinary Care',
  'Gas Station': 'Gas Station',
  'Florist': 'Florist',
  'Roofing Contractor': 'Roofing Contractor',
  'Clothing Store': 'Clothing Store',
  'Meal Delivery': 'Meal Delivery',
  'Moving Company': 'Moving Company',
  'Jewelry Store': 'Jewelry Store',
  'Hair Care': 'Hair Care',
  'Plumber': 'Plumber',
  'Shoe Store': 'Shoe Store',
  'Painter': 'Painter'
};

export const I18N_LANGUAGES: Dictionary<string> = {
  en: i18n('English'),
  ru: i18n('Russian'),
  de: i18n('German'),
  fr: i18n('French'),
  es: i18n('Spanish'),
  it: i18n('Italian'),
};

export const I18N_ERROR_MESSAGES = {
  credentials: i18n('Incorrect credentials'),
  passwordDoesNotMatch: i18n('Password doesn\'t match'),
  emailMustBeUnique: i18n('Email must be unique'),
  unselectedPin: 'Please, select pin on the map or use search',
  unresolvedLocation: 'Sorry, you cannot suggest location here. Try in the habitable area.',
  submittedProject: 'Congratulations! Your project is now submitted to vote! Start your campaign by sharing it! ',
  canNotSubmitProject: 'Project has been already submitted for voting',
  canNotMovePin: 'Sorry! This pin can\'t be moved.',
  singleProject: 'There is single project',
  singleEvent: 'There is single event',
  singleMessage: 'There is single idea',
  selectProject: 'Please, select a project or event.',
  moveForeignPin: 'You can\'t update pin of another user',
  locationHaveProject: 'Sorry! This pin is already have selected project which won in voting.',
  largeHeight: 'You can not suggest location at height more than 200 kilometers.',
  navHint: 'If you wish to visualise well your building,either use touch gestures or the CTRL button of your keyboard and turn around with your mouse.',
  addressNotFound: 'In the formulation entered, we cannot find the address, please try again with the correct name of the location',
  viewHint: 'To view project details click on an icon on the map.',
  createHint: 'To create a new project click on an icon in the left menu and choose location on the map.',
  zoomHint: 'If you wish to zoom in or zoom out the map use the buttons at the bottom right corner or click Navigation Hints menu item to get instructions for mouse and touch control.',
  subcategoryError: 'Another similar project is already built in this area. Please, join this project to support it or choose a new location for your project.'
};

export const I18N_REWARDS = {
  receive: 'Congratulations, you have received ${reward} points!',
  lost: 'Unfortunately, you have lost ${reward} points!'
};

export const I18N_MISSIONS = [
  'Support kids missing a school with an educational project.',
  'Help ill persons in difficulty with a new medical center.',
  'Bring cultural lights in darkness.',
  'Reduce CO2 emissions with a park in polluted area.',
  'Boost economic performance with a commercial center.',
  'Make citizens healthier by offering them a sport center.',
  'Help people in difficulty with a social center.',
  'Create some inspiration to others in the platform.',
  'Visit other people projects, vote.',
  'Promote projects by sharing them.',
  'Organize an event to make your city more lively.'
];

export const I18N_HEADERS = {
  mouse: 'Mouse',
  touchpad: 'Touchpad',
  touch: 'Touch',
  keyboard: 'Keyboard',
};

export const I18N_PUBLIC_CATEGORIES: Dictionary<string> = {
  'Sport Center': 'Sport Center',
  'Social Center': 'Social Center',
  'Commercial center': 'Commercial center',
  'Cultural Center': 'Cultural Center',
  'Medical Center': 'Medical Center',
  'Educational Center': 'Educational Center',
  'Hospital': 'Hospital'
};

export const I18N_PROJECT_ACTIONS = {
  cancel: 'Your ${projectType} is cancelled!',
  beforeCreate: 'You need to save your project to access the performance estimation and the possibility to submit it  to vote. As long as it is not submitted, you will still be able to suppress it.'
};

export const I18N_PROJECT_TYPES: Dictionary<string> = {
  'project': 'project',
  'business project': 'business project',
  'event': 'event',
  'idea': 'idea'
};

