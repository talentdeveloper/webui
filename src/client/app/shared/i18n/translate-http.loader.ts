import { Observable } from 'rxjs/Observable';
import { TranslateLoader } from '@ngx-translate/core';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';

export class TranslateHttpLoader implements TranslateLoader {
  constructor(private http: HttpClient, private prefixes: string[] = ['/assets/i18n/'], private suffix: string = '.json') {
  }

  public getTranslation(lang: string): Observable<any> {
      const requests: Observable<{}>[] = [];
      this.prefixes.forEach(prefix => {
        const url = `${prefix}${lang}${this.suffix}`;
        const request = this.http.get(url).map(response => response);
        requests.push(request);
      });

      return forkJoin(...requests)
        .map(res => {
          return _.merge(res[0], {_calendar: res[1]});
        });
  }
}
