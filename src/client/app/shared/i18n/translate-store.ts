import { CalendarLocale } from './calendar';
import { TranslateStore } from '@ngx-translate/core/src/translate.store';

export class CodemosTranslateStore extends TranslateStore {
  _calendar: CalendarLocale;
}
