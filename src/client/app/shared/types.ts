export declare type Dictionary<T> = {[key: string]: T};

export interface ApiErrorMessages {
  errorCodes: {[key: number]: string};
}
