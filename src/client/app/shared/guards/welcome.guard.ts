import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ROUTES } from '../constants/routes';
import { COOKIES } from '../constants/cookieNames';
import { SessionService } from '../services/session.service';
import { RedirectService } from '../services/redirect.service';
import { CookieService } from '../services/cookie.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/src/router_state';

@Injectable()
export class WelcomeGuard implements CanActivate {
  constructor(
    private sessionService: SessionService,
    private cookieService: CookieService,
    private redirectService: RedirectService,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.sessionService.isAuthorized() && !this.cookieService.get(COOKIES.visitedWelcomePage)) {
      this.redirectService.setRedirectUrl(state.url);
      this.router.navigate([ROUTES.welcome]);
      return false;
    }
    return true;
  }
}
