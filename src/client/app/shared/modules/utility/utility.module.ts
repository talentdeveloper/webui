import { ModuleWithProviders, NgModule } from '@angular/core';
import { LoggingService } from './service/logging.service';

@NgModule({
  imports: []
})
export class UtilityModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UtilityModule,
      providers: [
        LoggingService,
      ]
    };
  }
}
