import { Injectable } from '@angular/core';

@Injectable()
export class CordovaService {
  public cordova: any;
  public device: any;

  constructor() {
    this.cordova = (<any>window).cordova;
    this.device = (<any>window).device;
  }

  public isCordova(): boolean {
    return !!this.cordova;
  }

  public getPlatform(): string {
    return this.device ? this.device.platform : 'No Cordova app.';
  }

  public isIOS(): boolean {
    return this.getPlatform() === 'iOS';
  }

  public isAndroid(): boolean {
    return this.getPlatform() === 'Android';
  }

  public isConnectionEstablished(): boolean {
    const networkState = (<any>navigator).connection.type;
    return networkState !== Connection.NONE;
  }
}
