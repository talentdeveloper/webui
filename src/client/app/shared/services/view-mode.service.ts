import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class ViewModeService {
  public cancel: EventEmitter<null> = new EventEmitter<null>();
  private displayViewModeDialog: boolean;

  public showViewModeDialog() {
    this.displayViewModeDialog = true;
    return this.cancel;
  }

  public hideViewModeDialog() {
    this.displayViewModeDialog = false;
  }

  public getDisplayViewModeDialog(): boolean {
    return this.displayViewModeDialog;
  }
}
