import { Injectable } from '@angular/core';

@Injectable()
export class IdleService {
  private timeout: number;
  private timer: any;
  private cb: any;

  constructor() {
    window.addEventListener('mousemove', () => this.resetTimer());
    window.addEventListener('click', () => this.resetTimer());
    window.addEventListener('touchstart', () => this.resetTimer());
    window.addEventListener('touchend', () => this.resetTimer());
    window.addEventListener('touchcancel', () => this.resetTimer());
    window.addEventListener('touchmove', () => this.resetTimer());
  }

  public startTimer(cb: any, timeout: number) {
    this.cb = cb;
    this.timeout = timeout;

    this.timer = setTimeout(() => {
      this.cb();
      this.startTimer(this.cb, this.timeout);
    }, this.timeout);
  }

  public resetTimer() {
    this.clearTimer();
    this.timer = setTimeout(() => {
      this.cb();
      this.startTimer(this.cb, this.timeout);
    }, this.timeout);
  }

  public clearTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }
}
