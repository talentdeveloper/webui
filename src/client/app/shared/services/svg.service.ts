export class SvgService {
  static createIconWithColor(svgText: string, color: string) {
    const svgElement = document.createElement('svg');
    svgElement.innerHTML = svgText;
    const path = svgElement.getElementsByTagName('path')[0];
    path.setAttribute('fill', color);
    return svgElement;
  }
}
