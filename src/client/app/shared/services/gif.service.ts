import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs/Subscription';

export class GifService {
  private display = false;
  private subscription: Subscription;
  private gifType: string;

  public show(gifType: string) {
    this.gifType = gifType;
    this.display = true;

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = Observable.interval(3400)
      .subscribe(() => {
        this.hide();
        this.subscription.unsubscribe();
      });
  }

  public hide() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.display = false;
  }

  public getGifType() {
    return this.gifType;
  }

  public isVisible(): boolean {
    return this.display;
  }
}
