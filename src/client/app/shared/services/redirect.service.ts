import { Injectable } from '@angular/core';
import { NavigationExtras } from '@angular/router';

export class RedirectData {
  navigationExtras: NavigationExtras;
  redirectUrl: string;
}

@Injectable()
export class RedirectService {
  private redirectUrl: string;
  private navigationExtras: NavigationExtras;

  public getRedirectData(): RedirectData {
    const navigationExtras = this.navigationExtras || undefined;

    return {
      redirectUrl: this.redirectUrl,
      navigationExtras,
    };
  }

  public setRedirectUrl(redirectUrl: string, navigationExtras?: NavigationExtras) {
    this.navigationExtras = navigationExtras;
    this.redirectUrl = redirectUrl;
  }
}
