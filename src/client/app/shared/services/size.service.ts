import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SizeService {
  private width: number;
  private $menuWidth: BehaviorSubject<number> = new BehaviorSubject<number>(null);

  constructor() {
    this.setWidth();
    window.addEventListener('resize', () => this.setWidth());
  }

  public getWidth(): number {
    return this.width;
  }

  public isMobile(): boolean {
    return this.getWidth() < 1200;
  }

  public setMenuWidth(width: number): void {
    this.$menuWidth.next(width);
  }

  public getMenuWidth(): BehaviorSubject<number> {
    return this.$menuWidth;
  }

  private setWidth(): void {
    this.width = window.innerWidth;
  }

}
