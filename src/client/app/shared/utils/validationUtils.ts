import { ValidatorFn, Validators } from '@angular/forms';
import {
  validateLength,
  validateMaxLength,
  validateMaxValue,
  validateMaxYear,
  validateMinLength,
  validateMinValue
} from './validators';

export function getValidatorsFromValidationsObject(validationsObject: any): ValidatorFn {
  const validatorsArray: ValidatorFn[] = [];
  for (const validationName in validationsObject) {
    if (validationsObject.hasOwnProperty(validationName)) {
      const validationObject = validationsObject[validationName];
      if (validationObject.value !== false) {
        validatorsArray.push(getValidatorByName(validationName, validationObject));
      }
    }
  }

  return Validators.compose(validatorsArray);
}

export function getValidatorByName(validatorName: string, options?: any): ValidatorFn {
  switch (validatorName) {
    case 'required':
      return Validators.required;
    case 'requiredTrue':
      return Validators.requiredTrue;
    case 'length':
      return validateLength(options.value);
    case 'minLength':
      return validateMinLength(options.value);
    case 'maxLength':
      return validateMaxLength(options.value);
    case 'email':
      return Validators.email;
    case 'pattern':
      return Validators.pattern(options.value);
    case 'minValue':
      return validateMinValue(options.value);
    case 'maxValue':
      return validateMaxValue(options.value);
    case 'maxYear':
      return validateMaxYear(options.value);
    default:
      return null;
  }
}
