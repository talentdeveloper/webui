export function isNavigationButton(keyCode: number, isCtrlKey: boolean, isMetaKey: boolean): boolean {
  return (
    // Backspace and Tab
    (keyCode === 8 || keyCode === 9) ||
    // Delete, Insert, Home, End
    (keyCode === 46 || keyCode === 36 || keyCode === 35) ||
    // Arrows
    (keyCode >= 37 && keyCode <= 40) ||
    //  CTRL + A and CTRL + C and CTRL + X and CTRL + V
    ((keyCode === 65 || keyCode === 67 || keyCode === 88 || keyCode === 86) && (isCtrlKey || isMetaKey))
  );
}

export function isNumberButton(keyCode: number): boolean {
  return (keyCode >= 96 && keyCode <= 105 || keyCode >= 48 && keyCode <= 57);
}

const numberRegExp = new RegExp(/^\d+$/g);
export function isOnlyNumbersByRegExp(value: string): boolean {
  return numberRegExp.test(value);
}
