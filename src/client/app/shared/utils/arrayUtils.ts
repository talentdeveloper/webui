export function hasItemWithId(arr: Array<any>, id: any): boolean {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].id === id) {
      return true;
    }
  }
  return false;
}
