import { AbstractControl } from '@angular/forms';

export function validateLength(length: number) {
  return function (inputControl: AbstractControl) {
    if (inputControl.value && inputControl.value.length !== length && inputControl.value.length !== 0) {
      return {
        length: true,
      };
    } else {
      return null;
    }
  };
}

export function validateMinLength(minLength: number) {
  return function (inputControl: AbstractControl) {
    if (inputControl.value && inputControl.value.length < minLength && inputControl.value.length !== 0) {
      return {
        minLength: true
      };
    } else {
      return null;
    }
  };
}

export function validateMaxLength(maxLength: number) {
  return function (inputControl: AbstractControl) {
    if (inputControl.value && inputControl.value.length > maxLength) {
      return {
        maxLength: true
      };
    } else {
      return null;
    }
  };
}

export function validateMinValue(minValue: number) {
  return function (inputControl: AbstractControl) {
    if (inputControl.value && +inputControl.value < minValue) {
      return {
        minValue: true
      };
    } else {
      return null;
    }
  };
}

export function validateMaxValue(maxValue: number) {
  return function (inputControl: AbstractControl) {
    if (inputControl.value && +inputControl.value > maxValue) {
      return {
        maxValue: true
      };
    } else {
      return null;
    }
  };
}

export function validateMaxYear(maxYear: number) {
  return function (inputControl: AbstractControl): any {
    const date = new Date();
    date.setFullYear(date.getFullYear() - maxYear);
    if (date < new Date(inputControl.value)) {
      return {
        maxYear: true
      };
    } else {
      return null;
    }
  };
}
