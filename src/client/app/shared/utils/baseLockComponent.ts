export class BaseLockComponent {
  public isLocked: boolean;

  constructor() {
  }

  public lockComponent(): void {
    this.isLocked = true;
  }

  public unlockComponent(): void {
    this.isLocked = false;
  }
}
