import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES } from '../../../shared/constants/routes';
import { AuthService } from '../../../auth/services/auth.service';
import { SecurityPrincipal } from '../../../shared/models/SecurityPrincipal';

@Component({
  moduleId: module.id,
  selector: 'cd-user-info',
  templateUrl: 'user-info.component.html',
  styleUrls: ['user-info.component.css'],
})
export class UserInfoComponent {
  @Input() securityPrincipal: SecurityPrincipal;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {}

  goToProfile() {
    this.router.navigate([ROUTES.profile, this.securityPrincipal.getPrincipalId()]);
  }

  logout() {
    this.authService.logout()
      .subscribe();
  }

  login() {
    this.router.navigate([ROUTES.login]);
  }
}
