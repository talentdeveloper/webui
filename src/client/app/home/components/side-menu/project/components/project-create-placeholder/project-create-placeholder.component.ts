import { Component } from '@angular/core';
import { AbstractCreatePlaceholderComponent } from '../../../../../../shared/components/create-placeholder/create-placeholder.component';
import { NavigationService } from '../../../../../../shared/map/navigation.service';

@Component({
  moduleId: module.id,
  selector: 'cd-project-create-placeholder',
  templateUrl: 'project-create-placeholder.component.html'
})
export class ProjectCreatePlaceholderComponent extends AbstractCreatePlaceholderComponent {
  constructor(navigationService: NavigationService) {
    super(navigationService);
  }
}
