import { NgModule } from '@angular/core';
import { ButtonModule, DialogModule, SliderModule } from 'primeng/primeng';
import { SharedModule } from '../../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectService } from './project.service';
import { ProjectsApi } from '../../../../shared/api/service/projects';
import { ProjectCreateActivityComponent } from './activities/project-create/project-create.activity';
import { ProjectViewActivityComponent } from './activities/project-view/project-view.activity';
import { ProjectFormComponent } from './components/project-form/project-form.component';
import { ProjectTypeService } from './project-type.service';
import { ProjectResolver } from './project.resolver';
import { ProjectTypeApi } from '../../../../shared/api/service/project-type.api';
import { ProjectCategoryService } from './project-category.service';
import { ProjectCategoryApi } from '../../../../shared/api/service/project-category.api';
import { ProjectCreatePlaceholderComponent } from './components/project-create-placeholder/project-create-placeholder.component';

@NgModule({
  declarations: [
    ProjectCreateActivityComponent,
    ProjectViewActivityComponent,
    ProjectFormComponent,
    ProjectCreatePlaceholderComponent,
  ],
  imports: [
    SharedModule,
    TranslateModule.forChild(),
    DialogModule,
    ButtonModule,
    SliderModule,
  ],
  exports: [
    ProjectCreateActivityComponent,
    ProjectViewActivityComponent,
    ProjectCreatePlaceholderComponent,
  ],
  providers: [
    ProjectService,
    ProjectsApi,
    ProjectTypeService,
    ProjectTypeApi,
    ProjectResolver,
    ProjectCategoryService,
    ProjectCategoryApi,
  ]
})
export class ProjectModule {
}
