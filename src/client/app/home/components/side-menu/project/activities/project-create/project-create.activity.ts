import { Component } from '@angular/core';
import { LocationService } from '../../../location/location.service';
import { AbstractProjectCreateComponent } from '../../../../../../shared/components/project-create/project-create.component';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { TranslateService } from '@ngx-translate/core';
import { ProjectService } from '../../project.service';
import { Project } from '../../../../../../shared/api/models/project.model';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { I18N_ERROR_MESSAGES, I18N_PROJECT_ACTIONS } from '../../../../../../shared/i18n/elements';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { ProjectTypeService } from '../../project-type.service';
import { ProjectType } from '../../../../../../shared/api/models/project-type.model';
import { ProjectCategoryService } from '../../project-category.service';
import { ProjectCategory } from '../../../../../../shared/api/models/project-category.model';
import { NotificationType } from '../../../../../../shared/components/notification/notification-type.service';
import { SessionService } from '../../../../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-project-create',
  templateUrl: 'project-create.activity.html'
})
export class ProjectCreateActivityComponent extends AbstractProjectCreateComponent<Project> {
  public categories: ProjectCategory[];
  public subcategoryError: string;
  public LABELS = CONSTANTS.PROJECT.CREATE;

  constructor(protected locationService: LocationService,
              protected notificationService: NotificationService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected loadingIndicatorService: LoadingIndicatorService,
              protected navigationService: NavigationService,
              protected stateService: StateService,
              protected projectService: ProjectService,
              protected projectTypeService: ProjectTypeService,
              protected translate: TranslateService,
              private sessionService: SessionService,
              private categoryService: ProjectCategoryService) {
    super(
      router,
      route,
      projectService,
      loadingIndicatorService,
      locationService,
      notificationService,
      navigationService,
      stateService,
      projectTypeService,
      translate
    );
  }


  public getProjectModel(): Project {
    return super.getProjectModel();
  }

  protected beforeCreateProject() {
    this.route.queryParams.subscribe((params: any) => {
      this.projectTypeService.getProjectTypeByName(params.type)
        .subscribe((projectType: ProjectType) => {
          this.projectType = projectType;
          this.stateService.selectLocation(this.projectType.name);
          this.categoryService.getCategories()
            .subscribe((categories: ProjectCategory[]) => {
              this.categories = categories.filter((category: ProjectCategory) => category.type === this.projectType.name);
            });
        });
    });

    this.notificationService.show(I18N_PROJECT_ACTIONS.beforeCreate, NotificationType.NOTIFY, 7000);
  }

  protected onSubmitError() {
    this.subcategoryError = 'Another similar project is already built in this area.';
    this.translate.get(I18N_ERROR_MESSAGES.subcategoryError)
      .subscribe(res => {
        this.notificationService.show(res);
      });
  }

  protected createProject(): Project {
    const principal = this.sessionService.getSecurityPrincipal();
    return new Project({
      location: this.location,
      creator: {
        id: principal.getPrincipalId(),
        firstName: principal.getPrincipalFirstName(),
        lastName: principal.getPrincipalLastName(),
      }
    });
  }
}
