import { Injectable } from '@angular/core';
import { ProjectTypeApi } from '../../../../shared/api/service/project-type.api';
import { ApiResponseDto } from '../../../../shared/api/common/ApiResponseDto';
import { ProjectType } from '../../../../shared/api/models/project-type.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class ProjectTypeService {

  private projectTypes: ProjectType[];

  constructor(private projectTypeApi: ProjectTypeApi) {
  }

  public getProjectTypeByName(name: string): Observable<ProjectType> {
    return this.getAvailableProjectTypes().map((projectTypes: ProjectType[]) => projectTypes.find((projectType: ProjectType) => projectType.name === name));
  }

  public getAvailableProjectTypes(): Observable<Array<ProjectType>> {
    if (this.projectTypes) {
      return Observable.of(this.projectTypes);
    } else {
      return this.projectTypeApi.getAvailableProjectTypes()
        .map((response: ApiResponseDto<Array<ProjectType>>) => {
          this.projectTypes = response.payload.map((type: ProjectType) => new ProjectType(type));
          return this.projectTypes;
        })
        .catch(error => Observable.throw(error));
    }
  }
}
