import { Injectable } from '@angular/core';
import { ApiResponseDto } from '../../../../shared/api/common/ApiResponseDto';
import { Observable } from 'rxjs/Observable';
import { ProjectCategory } from '../../../../shared/api/models/project-category.model';
import { ProjectCategoryApi } from '../../../../shared/api/service/project-category.api';

@Injectable()
export class ProjectCategoryService {

  constructor(private projectCategoryApi: ProjectCategoryApi) {
  }

  public getCategories(): Observable<Array<ProjectCategory>> {
    return this.projectCategoryApi.getProjectCategories()
      .map((response: ApiResponseDto<Array<ProjectCategory>>) => {
        return response.payload.map((category: ProjectCategory) => new ProjectCategory(category));
      })
      .catch(error => Observable.throw(error));
  }

  public getCategory(id: string): Observable<ProjectCategory> {
    return this.projectCategoryApi.getProjectCategory(id)
      .map((response: ApiResponseDto<ProjectCategory>) => {
        return new ProjectCategory(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

}
