import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Message } from '../../../../shared/api/models/message';
import { IdeaService } from './services/idea.service';

@Injectable()
export class IdeaResolver implements Resolve<Message> {
  constructor(private ideaService: IdeaService) {
  }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Message> {
    return this.ideaService.getProject(route.params.id);
  }
}
