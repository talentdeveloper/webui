import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../../../../../../shared/api/models/message';
import { SaveMessageRequest } from '../../../../../../shared/api/models/savemessagerequest';
import { FormBuilder, FormGroup } from '@angular/forms';
import { VALIDATIONS } from '../../../../../../auth/constants/validations';
import { IdeaService } from '../../services/idea.service';
import { LocationService } from '../../../location/location.service';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { LocationInfo } from '../../../../../../shared/api/models/location-info.models';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { getValidatorsFromValidationsObject } from '../../../../../../shared/utils/validationUtils';
import { AbstractProjectFormComponent } from '../../../../../../shared/components/project-form/project-form.component';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { SessionService } from '../../../../../../shared/services/session.service';
import { SizeService } from '../../../../../../shared/services/size.service';
import { ConditionService } from '../../../../../../shared/services/condition.service';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { ProjectType } from '../../../../../../shared/api/models/project-type.model';

@Component({
  moduleId: module.id,
  selector: 'cd-inspire-form',
  templateUrl: 'inspire-form.component.html',
  styleUrls: ['inspire-form.component.css']
})

export class InspireFormComponent extends AbstractProjectFormComponent<Message, SaveMessageRequest> implements OnInit, OnDestroy {
  @Input() projectType: ProjectType;
  @Input() isLocked: boolean;

  public inspireForm: FormGroup;
  public location: LocationInfo;
  public badRequest: string;
  public VALIDATIONS: any = VALIDATIONS;
  public disabled: boolean;
  public LABELS = CONSTANTS.INSPIRE;

  constructor(
    protected inspireService: IdeaService,
    protected locationService: LocationService,
    protected promoteThemService: PromoteThemService,
    protected sessionService: SessionService,
    protected sizeService: SizeService,
    protected navigationService: NavigationService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected conditionService: ConditionService,
    private formBuilder: FormBuilder,
  ) {
    super(
      promoteThemService,
      inspireService,
      locationService,
      loadingIndicatorService,
      translate,
      notificationService,
      sessionService,
      sizeService,
      conditionService,
      navigationService
    );
    this.disabled = !this.isUserCreator();
  }

  public canBeModified(): boolean {
    return super.canBeModified();
  }

  public isUserCreator(): boolean {
    return super.isUserCreator();
  }

  public onShow() {
    super.onShow();
  }

  public onHide() {
    super.onHide();
  }

  public removeProject() {
    super.removeProject();
  }

  public nextProject() {
    super.nextProject();
  }

  public isVisible(): boolean {
    return super.isVisible();
  }

  public share() {
    return super.share(PROJECT_TYPES.IDEA);
  }

  public buildSaveRequest() {
    return new SaveMessageRequest({
      name: this.inspireForm.value.name,
      description: this.inspireForm.value.description || '',
      capacity: this.inspireForm.value.capacity,
      location: this.project.location.id
    });
  }

  public initForm() {
    this.inspireForm = this.formBuilder.group({
      name: [{
        value: this.project.name,
        disabled: this.disabled,
      }, getValidatorsFromValidationsObject(VALIDATIONS.inspire.name)],
      description: [{
        value: this.project.description,
        disabled: this.disabled,
      }, getValidatorsFromValidationsObject(VALIDATIONS.inspire.description)],
      capacity: [{
        value: this.project.capacity,
        disabled: this.disabled,
      }, getValidatorsFromValidationsObject(VALIDATIONS.inspire.capacity)],
    });
  }
}
