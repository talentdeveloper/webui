import { Component } from '@angular/core';
import { AbstractCreatePlaceholderComponent } from '../../../../../../shared/components/create-placeholder/create-placeholder.component';
import { NavigationService } from '../../../../../../shared/map/navigation.service';

@Component({
  moduleId: module.id,
  selector: 'cd-inspire-create-placeholder',
  templateUrl: 'inspire-create-placeholder.component.html'
})
export class InspireCreatePlaceholderComponent extends AbstractCreatePlaceholderComponent {
  constructor(navigationService: NavigationService) {
    super(navigationService);
  }
}
