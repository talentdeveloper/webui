import { Component } from '@angular/core';
import { LocationService } from '../../../location/location.service';
import { AbstractProjectCreateComponent } from '../../../../../../shared/components/project-create/project-create.component';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from '../../../../../../shared/api/models/message';
import { TranslateService } from '@ngx-translate/core';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { IdeaService } from '../../services/idea.service';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { ProjectTypeService } from '../../../project/project-type.service';
import { SessionService } from '../../../../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-inspire',
  templateUrl: 'inspire-create.activity.html'
})
export class InspireCreateActivityComponent extends AbstractProjectCreateComponent<Message> {

  public LABELS = CONSTANTS.INSPIRE.CREATE;

  protected projectTypeString = 'idea';

  constructor(protected locationService: LocationService,
              protected notificationService: NotificationService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected navigationService: NavigationService,
              protected loadingIndicatorService: LoadingIndicatorService,
              protected inspireService: IdeaService,
              protected stateService: StateService,
              protected projectTypeService: ProjectTypeService,
              protected translate: TranslateService,
              private sessionService: SessionService) {
    super(
      router,
      route,
      inspireService,
      loadingIndicatorService,
      locationService,
      notificationService,
      navigationService,
      stateService,
      projectTypeService,
      translate
    );
  }

  public cancel() {
    return super.cancel();
  }

  public getProjectModel(): Message {
    return super.getProjectModel();
  }

  protected createProject(): Message {
    const principal = this.sessionService.getSecurityPrincipal();
    return new Message({
      location: this.location,
      creator: {
        id: principal.getPrincipalId(),
        firstName: principal.getPrincipalFirstName(),
        lastName: principal.getPrincipalLastName(),
      }
    });
  }
}
