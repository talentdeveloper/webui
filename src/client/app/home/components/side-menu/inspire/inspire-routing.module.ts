import { Routes } from '@angular/router';
import { InspireViewActivityComponent } from './activities/inspire-view/inspire-view.activity';
import { InspireCreateActivityComponent } from './activities/inspire-create/inspire-create.activity';
import { AuthorizedGuard } from '../../../../shared/guards/authorized.guard';
import { IdeaResolver } from './inspire.resolver';

export const inspireRoutes: Routes = [
  {
    path: 'inspire',
    component: InspireCreateActivityComponent,
    canActivate: [AuthorizedGuard]
  },
  {
    path: 'inspire/:id',
    component: InspireViewActivityComponent,
    resolve: {
      project: IdeaResolver,
    }
  }
];
