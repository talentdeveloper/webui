import { Component, OnInit } from '@angular/core';
import { PromoteThemService } from './promote-them.service';
import { ShareProvider } from 'ngx-sharebuttons';
import { LocationService } from '../../location/location.service';
import { CONSTANTS } from '../../../../../shared/constants/constants';
import { LoadingIndicatorService } from '../../../../../shared/services/loading-indicator.service';
import { ShareProviderRequest } from '../../../../../shared/api/models/shareproviderrequest';
import { Router } from '@angular/router';
import { EventApi } from '../../../../../shared/api/service/event';
import { ProjectsApi } from '../../../../../shared/api/service/projects';
import { InspireApi } from '../../../../../shared/api/service/inspire';
import { PROJECT_TYPES } from '../../../../../shared/constants/projectTypes';
import { AbstractProject } from '../../../../../shared/api/common/AbstractProject';
import { NotificationService } from '../../../../../shared/components/notification/notification.service';
import { SessionService } from '../../../../../shared/services/session.service';
import { Config } from '../../../../../shared/config/env.config';
import { ROUTES } from '../../../../../shared/constants/routes';
import { Camera } from '../../../../../shared/models/camera';

@Component({
  moduleId: module.id,
  selector: 'cd-promote-them',
  templateUrl: 'promote-them.component.html',
  styleUrls: ['promote-them.component.css']
})
export class PromoteThemComponent implements OnInit {
  public LABELS = CONSTANTS.PROMOTE;
  public PROJECT_TYPES = PROJECT_TYPES;
  public project: AbstractProject;
  public type: string;
  public typeString: string;
  public url: string;

  constructor(
    private promoteThemService: PromoteThemService,
    private locationService: LocationService,
    private loadingIndicatorService: LoadingIndicatorService,
    private eventService: EventApi,
    private projectService: ProjectsApi,
    private inspireService: InspireApi,
    private router: Router,
    private sessionService: SessionService,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.project = this.promoteThemService.getProject();
    const camera = this.project.location.coordinates;
    this.type = this.promoteThemService.getProjectType();
    this.typeString = this.promoteThemService.getProjectTypeString();
    // tslint:disable-next-line
    this.url = `${Config.UI_URL}/#${ROUTES.camera}/${Camera.toFormatted(camera.latitude)}/${Camera.toFormatted(camera.longitude)}/15/${ROUTES.location}/${this.project.location.id}/${this.typeString}/${this.project.id}`;
  }

  shareSignal(provider: ShareProvider) {
    this.notificationService.show('Thanks for sharing!', null, 2000);
    let providerName;
    switch (provider) {
      case (ShareProvider.FACEBOOK): {
        providerName = 'facebook';
        break;
      }
      case (ShareProvider.TWITTER): {
        providerName = 'twitter';
        break;
      }
      case (ShareProvider.GOOGLEPLUS): {
        providerName = 'googleplus';
        break;
      }
      default: {
        providerName = '';
      }
    }
    const project = this.promoteThemService.getProject();
    const projectType = this.promoteThemService.getProjectType();
    const projectId = project.id;
    const shareProviderRequest = new ShareProviderRequest({provider: providerName});
    let service;

    if (projectType === this.PROJECT_TYPES.EVENT) {
      service = this.eventService;
    } else if (projectType === this.PROJECT_TYPES.IDEA) {
      service = this.inspireService;
    } else if (projectType === this.PROJECT_TYPES.PUBLIC_PROJECT) {
      service = this.projectService;
    }

    if (service && this.sessionService.isAuthorized()) {
      service.shareProject(projectId, shareProviderRequest)
        .subscribe(() => {
            this.promoteThemService.hide();
          },
          () => {
            this.promoteThemService.hide();
          });
    }
  }

  isVisible() {
    return this.promoteThemService.isVisible();
  }

  close() {
    this.promoteThemService.hide();
  }
}
