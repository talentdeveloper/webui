import { Component } from '@angular/core';
import { LocationService } from '../../../location/location.service';
import { Event } from '../../../../../../shared/api/models/event.model';
import { AbstractProjectCreateComponent } from '../../../../../../shared/components/project-create/project-create.component';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../../services/event.service';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { GifTypes } from '../../../../../../shared/constants/GifTypes';
import { GifService } from '../../../../../../shared/services/gif.service';
import { TranslateService } from '@ngx-translate/core';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { ProjectTypeService } from '../../../project/project-type.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { SessionService } from '../../../../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-event-create',
  templateUrl: 'event-create.activity.html'
})
export class EventCreateActivityComponent extends AbstractProjectCreateComponent<Event> {

  public LABELS = CONSTANTS.EVENT.CREATE;

  protected projectTypeString = 'event';

  constructor(protected locationService: LocationService,
              protected notificationService: NotificationService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected eventService: EventService,
              protected navigationService: NavigationService,
              protected stateService: StateService,
              protected projectTypeService: ProjectTypeService,
              protected loadingIndicatorService: LoadingIndicatorService,
              protected translate: TranslateService,
              private sessionService: SessionService,
              private gifService: GifService) {
    super(
      router,
      route,
      eventService,
      loadingIndicatorService,
      locationService,
      notificationService,
      navigationService,
      stateService,
      projectTypeService,
      translate
    );
  }

  public cancel() {
    return super.cancel();
  }

  public getProjectModel(): Event {
    return super.getProjectModel();
  }

  protected afterSaveResponse() {
    this.gifService.show(GifTypes.EVENT_SUBMIT);
  }

  protected createProject(): Event {
    const principal = this.sessionService.getSecurityPrincipal();
    return new Event({
      location: this.location,
      creator: {
        id: principal.getPrincipalId(),
        firstName: principal.getPrincipalFirstName(),
        lastName: principal.getPrincipalLastName(),
      }
    });
  }
}
