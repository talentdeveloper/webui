import { NgModule } from '@angular/core';
import { EventFormComponent } from './components/event-form/event-form.component';
import { CalendarModule, DialogModule } from 'primeng/primeng';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { SharedModule } from '../../../../shared/shared.module';
import { EventService } from './services/event.service';
import { EventApi } from '../../../../shared/api/service/event';
import { LocationService } from '../location/location.service';
import { EventCreateActivityComponent } from './activities/event-create/event-create.activity';
import { EventViewActivityComponent } from './activities/event-view/event-view.component';
import { SessionService } from '../../../../shared/services/session.service';
import { TranslateModule } from '@ngx-translate/core';
import { EventResolver } from './event.resolver';

@NgModule({
  declarations: [
    EventFormComponent,
    EventCreateActivityComponent,
    EventViewActivityComponent
  ],
  imports: [
    SharedModule,
    DialogModule,
    CalendarModule,
    TranslateModule.forChild(),
    NgxMyDatePickerModule.forRoot(),
  ],
  exports: [
    EventFormComponent,
    EventViewActivityComponent
  ],
  providers: [
    EventResolver,
    EventService,
    EventApi,
    LocationService,
    SessionService,
  ]
})
export class EventModule {
}
