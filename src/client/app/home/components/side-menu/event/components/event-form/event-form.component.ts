import { Component, Input } from '@angular/core';
import { LocationService } from '../../../location/location.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { SaveEventRequest } from '../../../../../../shared/api/models/saveeventrequest';
import { EventService } from '../../services/event.service';
import { Event } from '../../../../../../shared/api/models/event.model';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import * as moment from 'moment';
import { VALIDATIONS } from '../../../../../../auth/constants/validations';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { getValidatorsFromValidationsObject } from '../../../../../../shared/utils/validationUtils';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { AbstractProjectFormComponent } from '../../../../../../shared/components/project-form/project-form.component';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { SessionService } from '../../../../../../shared/services/session.service';
import { SizeService } from '../../../../../../shared/services/size.service';
import { ConditionService } from '../../../../../../shared/services/condition.service';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { ProjectType } from '../../../../../../shared/api/models/project-type.model';

@Component({
  moduleId: module.id,
  selector: 'cd-event-form',
  templateUrl: 'event-form.component.html',
  styleUrls: ['event-form.component.css']
})
export class EventFormComponent extends AbstractProjectFormComponent<Event, SaveEventRequest> {
  @Input() projectType: ProjectType;
  @Input() isLocked: boolean;

  public VALIDATIONS = VALIDATIONS;

  public LABELS = CONSTANTS.EVENT;

  public PROJECT_TYPES = PROJECT_TYPES;

  eventForm: FormGroup;

  disabled: boolean;

  positionLeft: number;

  dialogWidth = 490;

  private static timeIntervalValidator(AC: AbstractControl): any {
    const dateStart = AC.get('dateStart');
    const dateEnd = AC.get('dateEnd');
    if (dateStart.value && dateStart.value < new Date()) {
      dateStart.setErrors({timeInterval: true});
    }
    if (dateStart.value && dateEnd.value) {
      if (dateEnd.value <= dateStart.value) {
        dateEnd.setErrors({timeInterval: true});
        if (document.getElementById('dateEnd-field')) {
          document.getElementById('dateEnd-field').style.border = '1px solid red';
        }
      } else {
        dateEnd.setErrors(null);
        if (document.getElementById('dateEnd-field')) {
          document.getElementById('dateEnd-field').style.border = '1px solid #106cc8';
        }
      }
    }
  }

  constructor(protected locationService: LocationService,
              protected loadingIndicatorService: LoadingIndicatorService,
              protected sessionService: SessionService,
              protected sizeService: SizeService,
              protected notificationService: NotificationService,
              protected translate: TranslateService,
              protected eventService: EventService,
              protected promoteThemService: PromoteThemService,
              protected navigationService: NavigationService,
              protected conditionService: ConditionService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
  ) {
    super(promoteThemService, eventService, locationService, loadingIndicatorService, translate, notificationService, sessionService, sizeService, conditionService, navigationService);
    this.positionLeft = window.innerWidth - this.dialogWidth - 10;
    this.disabled = !this.isUserCreator();
  }

  public canBeModified(): boolean {
    return super.canBeModified();
  }

  public share(): void {
    super.share(this.PROJECT_TYPES.EVENT);
  }

  public submitToVote() {
    super.submitToVote();
  }

  public vote() {
    return super.vote();
  }

  public onShow(): void {
    super.onShow();
  }

  public onHide(): void {
    super.onHide();
  }

  public onCancel() {
    return super.onCancel();
  }

  public onSumbit() {
    return super.onSubmit();
  }

  public onHideConfirmDialog() {
    this.showConfirmDialog = false;
  }

  public onShowConfirmDialog() {
    this.showConfirmDialog = true;
  }

  public canProjectBeSubmittedToVote() {
    return super.canProjectBeSubmittedToVote();
  }

  public canProjectBeVoted() {
    return super.canProjectBeVoted();
  }

  public nextProject(): void {
    super.nextProject();
  }

  public removeProject(): void {
    super.removeProject();
  }

  public setEditMode(): void {
    return super.setEditMode();
  }

  public setViewMode(): void {
    return super.setViewMode();
  }

  public getDate(date: string): string {
    // TODO: clarify date format
    return moment(date).format('DD.MM.YYYY HH:mm');
  }

  public isUserCreator(): boolean {
    return super.isUserCreator();
  }

  public initForm(): void {
    this.eventForm = this.formBuilder.group({
      name: [{
        value: this.project.name,
        disabled: this.disabled,
      }, getValidatorsFromValidationsObject(VALIDATIONS.event.name)],
      description: [{
        value: this.project.description,
        disabled: this.disabled
      }, getValidatorsFromValidationsObject(VALIDATIONS.event.description)],
      dateStart: [{
        value: this.project.startDate ? new Date(this.project.startDate) : '',
        disabled: this.disabled
      }, getValidatorsFromValidationsObject(VALIDATIONS.event.startDate)],
      dateEnd: [{
        value: this.project.endDate ? new Date(this.project.endDate) : '',
        disabled: this.disabled
      }, getValidatorsFromValidationsObject(VALIDATIONS.event.endDate)],
      capacity: [{
        value: this.project.capacity,
        disabled: this.disabled
      }, getValidatorsFromValidationsObject(VALIDATIONS.event.capacity)],
    }, {
      validator: EventFormComponent.timeIntervalValidator
    });
    this.eventForm.valueChanges.subscribe(() => this.badRequest = null);
  }

  public dateStartColors(): void {
    if (this.eventForm.get('dateStart').invalid) {
      document.getElementById('dateStart-field').style.border = '1px solid red';
    } else {
      document.getElementById('dateStart-field').style.border = '1px solid #106cc8';
    }
  }

  public dateEndColors(): void {
    if (this.eventForm.get('dateEnd').invalid) {
      document.getElementById('dateEnd-field').style.border = '1px solid red';
    } else {
      document.getElementById('dateEnd-field').style.border = '1px solid #106cc8';
    }
  }

  public buildSaveRequest(): SaveEventRequest {
    return new SaveEventRequest({
      name: this.eventForm.value.name,
      description: this.eventForm.value.description || '',
      startDate: moment(this.eventForm.value.dateStart).startOf('minute').format(),
      endDate: moment(this.eventForm.value.dateEnd).startOf('minute').format(),
      location: this.project.location.id,
      capacity: this.eventForm.value.capacity
    });
  }
}
