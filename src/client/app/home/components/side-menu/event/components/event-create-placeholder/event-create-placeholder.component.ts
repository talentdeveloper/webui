import { Component } from '@angular/core';
import { AbstractCreatePlaceholderComponent } from '../../../../../../shared/components/create-placeholder/create-placeholder.component';
import { NavigationService } from '../../../../../../shared/map/navigation.service';

@Component({
  moduleId: module.id,
  selector: 'cd-event-create-placeholder',
  templateUrl: 'event-create-placeholder.component.html'
})
export class EventCreatePlaceholderComponent extends AbstractCreatePlaceholderComponent {
  constructor(navigationService: NavigationService) {
    super(navigationService);
  }
}
