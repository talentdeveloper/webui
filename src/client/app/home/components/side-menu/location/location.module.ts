import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { LocationDetailsComponent } from './location-details/location-details.component';
import { LocationApi } from '../../../../shared/api/service/location';
import { LocationService } from './location.service';
import { SharedModule } from '../../../../shared/shared.module';
import { DialogModule } from 'primeng/primeng';
import { LocationResolver } from './location.resolver';

@NgModule({
  imports: [
    SharedModule,
    DialogModule,
    TranslateModule.forChild()
  ],
  declarations: [
    LocationDetailsComponent
  ],
  exports: [
    LocationDetailsComponent,
  ],
  providers: [
    LocationApi,
    LocationResolver,
    LocationService
  ]
})
export class LocationModule {
}
