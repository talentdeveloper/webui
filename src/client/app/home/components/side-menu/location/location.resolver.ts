import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Location } from '../../../../shared/api/models/location.models';
import { Observable } from 'rxjs/Observable';
import { LocationService } from './location.service';

@Injectable()
export class LocationResolver implements Resolve<Location> {
  constructor(private locationService: LocationService) {
  }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Location> {
    return this.locationService.getLocation(route.params.id)
      .map((location: Location) => {
        return location;
      });
  }
}
