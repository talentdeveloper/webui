import { Routes } from '@angular/router';
import { LocationDetailsComponent } from './location-details/location-details.component';
import { projectRoutes } from '../project/project-routing.module';
import { eventRoutes } from '../event/event-routing.module';
import { inspireRoutes } from '../inspire/inspire-routing.module';
import { LocationResolver } from './location.resolver';

export const locationRoutes: Routes = [
  {
    path: 'location/:id',
    component: LocationDetailsComponent,
    children: [
      ...projectRoutes,
      ...eventRoutes,
      ...inspireRoutes,
    ],
    resolve: {
      location: LocationResolver
    },
    runGuardsAndResolvers: 'always'
  },
];
