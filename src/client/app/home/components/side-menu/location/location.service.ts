import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FindNearestLocationRequest } from '../../../../shared/api/models/findnearestlocationrequest';
import { LoadingIndicatorService } from '../../../../shared/services/loading-indicator.service';
import { ApiResponseDto } from '../../../../shared/api/common/ApiResponseDto';
import { SessionService } from '../../../../shared/services/session.service';
import { LocationInfo } from '../../../../shared/api/models/location-info.models';
import { LocationApi } from '../../../../shared/api/service/location';
import { Location } from '../../../../shared/api/models/location.models';
import { Point } from '../../../../shared/api/models/point.model';
import { LoggingService } from '../../../../shared/modules/utility/service/logging.service';
import { Area } from '../../../../shared/models/area';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/catch';

@Injectable()
export class LocationService {

  constructor(
    private logger: LoggingService,
    private locationApi: LocationApi,
  ) { }

  /**
   * Query for location
   *
   * @param {string} id - unique id of location to load
   * @returns {Observable<Location>} - promise that will resolve location object
   */
  public getLocation(id: string): Observable<Location> {
    return this.locationApi.getLocation(id)
      .map((response: ApiResponseDto<Location>) => {
        return new Location(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  /**
   * Create pinned location
   *
   * @param {Point} point - coordinate of the new pin
   */
  public addLocation(point: Point): Observable<Location> {
    return this.locationApi.addLocation(point)
      .map((response: ApiResponseDto<Location>) => {
        return new Location(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  /**
   * Update existing location
   *
   * @param {string} id  - unique id of location that needs to be updated
   * @param {Point} location - new coordinate of the pinned location
   */
  public updateLocation(id: string, location: Point) {
    this.locationApi.updateLocation(id, location)
      .subscribe(
        () => {
        },
        (error) => this.logger.error(error),
      );
  }

  /**
   * Remove existing location
   *
   * @param {string} id - unique id of location that needs to be removed
   */
  public removeLocation(id: string) {
    return this.locationApi.removeLocation(id)
      .catch(error => Observable.throw(error))
      .share();

  }

  /**
   * Find nearest to the specified coordinates pinned location
   *
   * NOTE: Currently not in use
   *
   * @param {FindNearestLocationRequest} visitedLocationsDTO - list of location to exclude from search
   * @returns {Observable<Location>} -  promise that will resolve location object
   */
  public findNearestLocation(visitedLocationsDTO: FindNearestLocationRequest): Observable<Location> {
    return this.locationApi.getNearestLocation(visitedLocationsDTO)
      .map((response: ApiResponseDto<Location>) => {
        return response.payload;
      });
  }

  /**
   *  Query for pinned location that match specified area
   *
   * @param area
   */
  public getLocationsInArea(area: Area): Observable<Array<LocationInfo>> {
    return this.locationApi.getLocations(area.latitude, area.longitude, area.radius)
      .map((response: ApiResponseDto<LocationInfo[]>) => {
        return response.payload.map(location => {
          return new LocationInfo(location);
        });
      });
  }

}
