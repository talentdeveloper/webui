import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { NavigationService } from '../../../shared/map/navigation.service';
import { Camera } from '../../../shared/models/camera';
import { ROUTES } from '../../../shared/constants/routes';
import { Subscription } from 'rxjs/Subscription';

@Component({
  moduleId: module.id,
  selector: 'cd-camera',
  templateUrl: 'camera.component.html',
  styleUrls: ['camera.component.css'],
})
export class CameraComponent implements OnInit, OnDestroy {
  private camera: Camera;
  private urlSegments: string[];
  private cameraChangedSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navigationService: NavigationService,
  ) {}

  getUrlSegments(root: any) {
    if (root && root.segments) {
      root.segments.forEach((segment: UrlSegment) => {
        this.urlSegments.push(segment.path);
      });
      return this.urlSegments;
    } else return [];
  }

  ngOnInit() {

    this.cameraChangedSubscription = this.navigationService.cameraChanged$
      .subscribe((camera: Camera) => {
        if (camera) {
          this.camera = camera;
          this.urlSegments = [];
          const parsedUrl = this.router.parseUrl(this.router.url);
          const queryParams = parsedUrl.queryParams ? parsedUrl.queryParams : null;
          const segments = this.getUrlSegments(parsedUrl.root.children.primary).slice(4) || [];
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ...segments,
          ], {
            queryParams
          });
        }
      });

    this.route.params
      .subscribe((params: any) => {
        if (params.lat && params.lng && params.zoom) {
          const camera = new Camera(params.lat, params.lng, params.zoom);
          if (!this.camera || (this.camera && !this.camera.equals(camera))) {
            this.navigationService.flyToByCoordinates(camera);
          }
        }
      });

  }

  ngOnDestroy(): void {
    this.cameraChangedSubscription.unsubscribe();
  }
}
