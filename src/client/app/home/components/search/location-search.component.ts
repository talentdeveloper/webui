import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SearchService } from '../../services/search.service';
import { LocationService } from '../side-menu/location/location.service';
import { NotificationService } from '../../../shared/components/notification/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { I18N_ERROR_MESSAGES } from '../../../shared/i18n/elements';
import { NavigationService } from '../../../shared/map/navigation.service';
import { Camera } from '../../../shared/models/camera';

@Component({
  moduleId: module.id,
  selector: 'cd-search',
  templateUrl: 'location-search.component.html',
  styleUrls: ['location-search.component.css'],
})
export class LocationSearchComponent implements OnInit {

  public searchForm: FormGroup;

  public showError = false;

  @ViewChild('searchInput')
  public searchInput: ElementRef;

  constructor(private formBuilder: FormBuilder,
              private searchService: SearchService,
              private navigationService: NavigationService,
              private locationService: LocationService,
              private notificationService: NotificationService,
              private translate: TranslateService) {
  }

  public ngOnInit() {
    this.buildSearchForm();
    this.searchForm.valueChanges.subscribe(val => this.showError = false);
  }

  public search() {
    if (this.searchForm.value.query) {
      this.searchInput.nativeElement.blur();
      this.onSubmit();
    }
  }

  public onSubmit() {
    this.searchService.searchByAddress(this.searchForm.value.query)
      .subscribe(
        (searchResults) => {
          const camera = new Camera(Camera.toFormatted(searchResults.coordinates.latitude), Camera.toFormatted(searchResults.coordinates.longitude));
          this.navigationService.flyToByCoordinates(camera);
        },
        (error) => {
          this.showError = true;
          this.translate.get(I18N_ERROR_MESSAGES.addressNotFound).subscribe(message => this.notificationService.show(message));
        }
      );
  }

  private buildSearchForm() {
    this.searchForm = this.formBuilder.group({query: ['', []]});
  }
}
