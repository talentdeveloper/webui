import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ProjectTypeService } from '../side-menu/project/project-type.service';
import { ROUTES } from '../../../shared/constants/routes';
import { ProjectService } from '../side-menu/project/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ProjectType } from '../../../shared/api/models/project-type.model';
import { SizeService } from '../../../shared/services/size.service';
import { Observable } from 'rxjs/Observable';
import { NavigationService } from '../../../shared/map/navigation.service';
import { Camera } from '../../../shared/models/camera';
import { Subscription } from 'rxjs/Subscription';
import { StateService } from '../../../shared/services/state.service';
import * as _ from 'lodash';

@Component({
  moduleId: module.id,
  selector: 'cd-project-menu',
  templateUrl: 'project-menu.component.html',
  styleUrls: ['project-menu.component.css'],
})
export class ProjectMenuComponent implements OnInit, OnDestroy {

  public projectTypes: Array<ProjectType>;

  private selectLocationSubscription: Subscription;
  private closeLocationSubscription: Subscription;

  @ViewChild('menuElement')
  public menu: ElementRef;

  constructor(private projectTypeService: ProjectTypeService,
              private projectService: ProjectService,
              private router: Router,
              private route: ActivatedRoute,
              private navigationService: NavigationService,
              private translate: TranslateService,
              private sizeService: SizeService,
              private stateService: StateService) {
    this.translate.onLangChange.subscribe(() => {
      this.updateMenuWidth();
    });
  }

  public ngOnInit() {
    this.projectTypeService.getAvailableProjectTypes()
      .subscribe((projectTypes: Array<ProjectType>) => {
        this.projectTypes = projectTypes;
        this.updateMenuWidth();
        this.enableMenuItems();
      });

    this.selectLocationSubscription = this.stateService.selectLocation$
      .subscribe((type: string) => {
        this.disableMenuItems(type);
      });

    this.closeLocationSubscription = this.stateService.closeLocation$
      .subscribe(() => {
        this.enableMenuItems();
      });
  }

  public ngOnDestroy(): void {
    this.selectLocationSubscription.unsubscribe();
    this.closeLocationSubscription.unsubscribe();
  }

  public createProject(type: string) {
    const camera = this.navigationService.getCameraPosition();

    // TODO Find how to do this better
    // Checking if there is resolved location data
    if (this.route.snapshot.children[0] && this.route.snapshot.children[0].children[0] && this.route.snapshot.children[0].children[0].data.location) {
      const locationId = this.route.snapshot.children[0].children[0].data.location.id;
      switch (type) {
        case 'park':
        case 'sport':
        case 'social':
        case 'business':
        case 'culture':
        case 'health':
        case 'education': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.location,
            locationId,
            ROUTES.project
          ], {
            queryParams: {
              type,
            }
          });
          break;
        }
        case 'event': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.location,
            locationId,
            ROUTES.event
          ]);
          break;
        }
        case 'idea': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.location,
            locationId,
            ROUTES.inspire
          ]);
          break;
        }
      }
    } else {
      switch (type) {
        case 'park':
        case 'sport':
        case 'social':
        case 'business':
        case 'culture':
        case 'health':
        case 'education': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.project
          ], {
            queryParams: {
              type,
            }
          });
          break;
        }
        case 'event': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.event,
          ]);
          break;
        }
        case 'idea': {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.inspire,
          ]);
          break;
        }
      }
    }
  }

  private updateMenuWidth() {
    const subscription = Observable.interval(300)
      .subscribe(() => {
        if (this.menu) {
          this.sizeService.setMenuWidth(this.menu.nativeElement.getBoundingClientRect().width);
        }
        subscription.unsubscribe();
      });
  }

  private disableMenuItems(type: string) {
    _.forEach(this.projectTypes, projectType => {
      projectType.disabled = type && projectType.name !== type;
    });
  }

  private enableMenuItems() {
    _.forEach(this.projectTypes, projectType => {
      projectType.disabled = false;
    });
  }
}
