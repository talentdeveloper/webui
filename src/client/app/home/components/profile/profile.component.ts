import { SecurityPrincipal } from '../../../shared/models/SecurityPrincipal';
import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SessionService } from '../../../shared/services/session.service';
import { Project } from '../../../shared/api/models/project.model';
import { ProjectService } from '../side-menu/project/project.service';
import { ROUTES } from '../../../shared/constants/routes';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { CONSTANTS } from '../../../shared/constants/constants';
import { Event } from '../../../shared/api/models/event.model';
import { Message } from '../../../shared/api/models/message';
import { AbstractProject } from '../../../shared/api/common/AbstractProject';
import { PromoteThemService } from '../side-menu/promote/promote-them/promote-them.service';
import { PROJECT_TYPES } from '../../../shared/constants/projectTypes';
import { EventService } from '../side-menu/event/services/event.service';
import { IdeaService } from '../side-menu/inspire/services/idea.service';
import { LocationService } from '../side-menu/location/location.service';
import { I18N_ERROR_MESSAGES } from '../../../shared/i18n/elements';
import { NotificationService } from '../../../shared/components/notification/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { hasItemWithId } from '../../../shared/utils/arrayUtils';
import { AuthService } from '../../../auth/services/auth.service';
import { ConditionService } from '../../../shared/services/condition.service';
import { ProjectType } from '../../../shared/api/models/project-type.model';
import { ProjectTypeService } from '../side-menu/project/project-type.service';
import { Observable } from 'rxjs/Observable';
import { Camera } from '../../../shared/models/camera';
import { StateService } from '../../../shared/services/state.service';
import { RedirectService } from '../../../shared/services/redirect.service';
import { ViewModeService } from '../../../shared/services/view-mode.service';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';

@Component({
  moduleId: module.id,
  selector: 'cd-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.css'],
})
export class ProfileComponent extends BaseLockComponent implements OnInit, OnChanges {
  @Input() public visible: boolean;
  @Input() public securityPrincipal: SecurityPrincipal = null;
  @Input() public set projects(projects: Project[]) {
    this._projects = projects;
    if (this._projects && this._projects.length > 0) {
      this.bestProjects = this._projects.sort((a: Project, b: Project) => b.performance - a.performance).slice(0, 3);
    } else {
      this.bestProjects = [];
    }
  }
  public get projects() {
    return this._projects;
  }
  @Input() public events: Event[] = [];
  @Input() public ideas: Message[] = [];

  @Output() public updateProjects: EventEmitter<string> = new EventEmitter<string>();
  @Output() public show: EventEmitter<null> = new EventEmitter();
  @Output() public hide: EventEmitter<null> = new EventEmitter();
  @Output() public showUpdateDialog: EventEmitter<null> = new EventEmitter();
  @Output() public showPasswordDialog: EventEmitter<null> = new EventEmitter();
  @Output() public showNotificationsDialog: EventEmitter<null> = new EventEmitter<null>();

  public bestProjects: Project[];
  public PROJECT_TYPES = PROJECT_TYPES;
  public projectTypes: Array<any> = [];
  public selectedCategory: string;
  public ownId: string;
  public isIE11: boolean;
  public trophies: number[];
  public LABELS: any = CONSTANTS.PROFILE;
  public CONSTANTS = CONSTANTS.PROFILE;
  public ROUTES = ROUTES;
  public showConfirmDialog = false;
  public confirmDialogLeftPosition: number;
  public showProfileSettings: boolean;
  private _selectedProject: AbstractProject;
  private _projects: Project[] = [];

  @HostListener('document:click', ['$event']) clickOutside(event: any) {
    this.showProfileSettings = false;
  }

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private sessionService: SessionService,
    private router: Router,
    private promoteThemService: PromoteThemService,
    private projectService: ProjectService,
    private projectTypeService: ProjectTypeService,
    private eventService: EventService,
    private inspireService: IdeaService,
    private locationService: LocationService,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private conditionService: ConditionService,
    private stateService: StateService,
    private redirectService: RedirectService,
    private viewModeService: ViewModeService,
  ) {
    super();
    if (this.sessionService.isAuthorized()) {
      this.ownId = this.sessionService.getSecurityPrincipal().getPrincipalId();
    }
    this.isIE11 = !!(window as any).MSInputMethodContext && !!(document as any).documentMode;
    this.confirmDialogLeftPosition = (window.innerWidth - this.confirmDialogLeftPosition) / 2;
  }

  public ngOnInit() {
    this.trophies = _.range(this.securityPrincipal.getPrincipalTrophiesCount());
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.collectProjectTypes([...this.projects, ...this.events, ...this.ideas]);
  }

  public share(project: AbstractProject) {
    if (!this.sessionService.isAuthorized()) {
      this.redirectService.setRedirectUrl(this.router.url);
      this.viewModeService.showViewModeDialog();
    } else {
      if (project instanceof Project) {
        this.promoteThemService.show(project, this.PROJECT_TYPES.PUBLIC_PROJECT);
      } else if (project instanceof Event) {
        this.promoteThemService.show(project, this.PROJECT_TYPES.EVENT);
      } else if (project instanceof Message) {
        this.promoteThemService.show(project, this.PROJECT_TYPES.IDEA);
      }
    }
  }

  public vote(project: AbstractProject) {
    this.lockComponent();
    const service: any = this.resolveProjectService(project);
    const projects: AbstractProject[] = this.getProjectsArray(project);
    service.vote(project.id)
      .finally(() => this.unlockComponent())
      .subscribe((responseProject: AbstractProject) => {
        const foundIndex = projects.findIndex(proj => proj === project);
        projects[foundIndex] = responseProject;
        this.updateProjects.emit(this.conditionService.getProjectTypeString(project));
      });
  }

  public submitToVote() {
    this.lockComponent();
    this.stateService.hideOverlayDialog();
    const service: any = this.resolveProjectService(this._selectedProject);
    const projects: AbstractProject[] = this.getProjectsArray(this._selectedProject);
    service.submitToVote(this._selectedProject.id).subscribe(
      (responseProject: AbstractProject) => {
          this.stateService.hideOverlayDialog();
          this.showConfirmDialog = false;
          const foundIndex = projects.findIndex(proj => proj === this._selectedProject);
          projects[foundIndex] = responseProject;
          this.updateProjects.emit(this.conditionService.getProjectTypeString(this._selectedProject));
          this.translate.get(I18N_ERROR_MESSAGES.submittedProject)
            .subscribe(res => {
              this.notificationService.show(res);
            });
        },
        (error: any) => {
          if (error.errorCode === 4) {
            this.translate.get(I18N_ERROR_MESSAGES.canNotSubmitProject)
              .subscribe(res => {
                this.notificationService.show(res);
              });
          }
        }, () => this.unlockComponent());
  }

  public resolveProjectService(project: AbstractProject) {
    if (project instanceof Project) {
      return this.projectService;
    } else if (project instanceof Event) {
      return this.eventService;
    } else if (project instanceof Message) {
      return this.inspireService;
    }
    return null;
  }

  public getProjectsArray(project: AbstractProject) {
    if (project instanceof Project) {
      const categoryIndex = this.projectTypes.findIndex((category: any) => category.name === project.category.type);
      return this.projectTypes[categoryIndex].items;
    } else if (project instanceof Event) {
      const categoryIndex = this.projectTypes.findIndex((category: any) => category.name === 'event');
      return this.projectTypes[categoryIndex].items;
    } else if (project instanceof Message) {
      const categoryIndex = this.projectTypes.findIndex((category: any) => category.name === 'idea');
      return this.projectTypes[categoryIndex].items;
    }
    return null;
  }

  public getProjectLabels(project: AbstractProject) {
    if (project instanceof Project) {
      return CONSTANTS.PROJECT.CONFIRM_DIALOG;
    } else if (project instanceof Event) {
      return CONSTANTS.EVENT.CONFIRM_DIALOG;
    } else if (project instanceof Message) {
      return CONSTANTS.INSPIRE.CONFIRM_DIALOG;
    }
    return null;
  }

  public onHideConfirmDialog() {
    this.stateService.hideOverlayDialog();
    this._selectedProject = null;
    this.showConfirmDialog = false;
  }

  public onShowConfirmDialog(project: AbstractProject) {
    this.stateService.showOverlayDialog();
    this.LABELS.CONFIRM_DIALOG = this.getProjectLabels(project);
    this._selectedProject = project;
    this.showConfirmDialog = true;
  }

  public isUserCreator(project: AbstractProject): boolean {
    return this.sessionService.getSecurityPrincipal().isItMy(project);
  }

  public setCategory(categoryId: string) {
    this.selectedCategory = categoryId;
  }

  public getProjectTypeImageUrlByType(type: string): Observable<string> {
    return this.projectTypeService.getProjectTypeByName(type)
      .map((projectType: ProjectType) => projectType.url);
  }

  public getProjectTypeColorByType(type: string): Observable<string> {
    return this.projectTypeService.getProjectTypeByName(type)
      .map((projectType: ProjectType) => projectType.color);
  }

  public getSelectedCategory(): string {
    return this.selectedCategory;
  }

  public onShow() {
    this.show.emit();
  }

  public onHide() {
    this.hide.emit();
  }

  public onShowUpdateDialog() {
    this.showUpdateDialog.emit();
  }

  public onShowPasswordDialog() {
    this.showPasswordDialog.emit();
  }

  public onShowNotificationsDialog() {
    this.showNotificationsDialog.emit();
  }

  public openBestProject(project: Project) {
    this.openProject(project);
  }

  public openProject(project: AbstractProject) {
    const camera = new Camera(project.location.coordinates.latitude, project.location.coordinates.longitude);
    const routerParams = [
      ROUTES.camera,
      Camera.toFormatted(camera.latitude),
      Camera.toFormatted(camera.longitude),
      Camera.toFormatted(camera.zoom),
      ROUTES.location,
      project.location.id
    ];

    if (project instanceof Project) {
      routerParams.push(ROUTES.project);
    } else if (project instanceof Event) {
      routerParams.push(ROUTES.event);
    } else if (project instanceof Message) {
      routerParams.push(ROUTES.inspire);
    }

    routerParams.push(project.id);

    this.router.navigate(routerParams);
  }

  public toggleDropdown(event: any) {
    this.showProfileSettings = !this.showProfileSettings;
  }

  public dropdownWrapperClick(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }

  public logout() {
    this.authService.logout().subscribe();
  }

  public canAbstractProjectBeSubmittedToVote(project: AbstractProject): boolean {
    return this.conditionService.canAbstractProjectBeSubmittedToVote(project);
  }

  private collectProjectTypes(projects: AbstractProject[]) {
    const types: any = [];
    for (let i = 0; i < projects.length; i++) {
      const typeName = projects[i].location.type;
      this.projectTypeService.getProjectTypeByName(typeName).subscribe(type => {
        if (!hasItemWithId(types, type.id)) {
          types.push({
            id: type.id,
            name: type.name,
            url: type.url,
            color: type.color,
            items: [projects[i]],
            order: type.order
          });
        } else {
          types.find((type: ProjectType) => type.name === projects[i].location.type).items.push(projects[i]);
        }
      });
    }
    this.projectTypes = types;
    this.projectTypes = _.uniqBy(this.projectTypes, type => type.name);
    this.projectTypes = _.orderBy(this.projectTypes, ['order']);
  }

}
