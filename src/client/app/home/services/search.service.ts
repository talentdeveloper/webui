import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiResponseDto } from '../../shared/api/common/ApiResponseDto';
import { SearchApi } from '../../shared/api/service/search';
import { SearchResult } from '../../shared/api/models/searchresult';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {

  constructor(private searchApi: SearchApi) {
  }

  public searchByAddress(address: string): Observable<SearchResult> {
    return this.searchApi.searchByAddress(address)
      .map((response: ApiResponseDto<SearchResult>) => {
        return response.payload;
      });
  }
}
