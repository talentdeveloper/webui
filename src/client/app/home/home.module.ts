import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { HomeActivityComponent } from './activities/home.activity';
import {
  ButtonModule,
  DialogModule,
  DropdownModule,
  InputTextareaModule,
  InputTextModule,
  PanelMenuModule,
  SliderModule
} from 'primeng/primeng';
import { LocationModule } from './components/side-menu/location/location.module';
import { AuthorizedGuard } from '../shared/guards/authorized.guard';
import { locationRoutes } from './components/side-menu/location/location-routing.module';
import { LoadingIndicatorComponent } from '../shared/components/loading-indicator/loading-indicator.component';
import { NotificationComponent } from '../shared/components/notification/notification.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { SessionService } from '../shared/services/session.service';
import { ProjectService } from './components/side-menu/project/project.service';
import { ProjectsApi } from '../shared/api/service/projects';
import { UserApi } from '../shared/api/service/user';
import { UserService } from './services/user.service';
import { EventModule } from './components/side-menu/event/event.module';
import { ScoreComponent } from './components/score/score.component';
import { InspireModule } from './components/side-menu/inspire/inspire.module';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileEditActivityComponent } from './activities/profile-edit/profile-edit.activity';
import { ProfileViewActivityComponent } from './activities/profile-view/profile-view.activity';
import { PromoteThemComponent } from './components/side-menu/promote/promote-them/promote-them.component';
import { PromoteThemService } from './components/side-menu/promote/promote-them/promote-them.service';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { HttpModule } from '@angular/http';
import { TranslateModule } from '@ngx-translate/core';
import { GifComponent } from '../shared/components/gif/gif.component';
import { LanguageService } from '../shared/services/language.service';
import { GifService } from '../shared/services/gif.service';
import { MenuPanelModule } from '../shared/components/menu-panel/menu-panel.module';
import { LocationSearchComponent } from './components/search/location-search.component';
import { SearchService } from './services/search.service';
import { SearchApi } from '../shared/api/service/search';
import { CategoryApi } from '../shared/api/service/category';
import { ProjectModule } from './components/side-menu/project/project.module';
import { ProjectMenuComponent } from './components/project-menu/project-menu.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { MapboxNavigatorComponent } from './components/mapbox-navigator/mapbox-navigator.component';
import { CameraComponent } from './components/camera/camera.component';
import { ContextHolderService } from '../shared/services/context-holder.service';
import { ProfileWrapperComponent } from './activities/profile-wrapper/profile-wrapper.component';
import { ProjectCreatePlaceholderComponent } from './components/side-menu/project/components/project-create-placeholder/project-create-placeholder.component';
import { EventCreatePlaceholderComponent } from './components/side-menu/event/components/event-create-placeholder/event-create-placeholder.component';
import { InspireCreatePlaceholderComponent } from './components/side-menu/inspire/components/inspire-create-placeholder/inspire-create-placeholder.component';
import { WelcomeActivityComponent } from '../static/activities/welcome/welcome.activity';
import { WelcomeGuard } from '../shared/guards/welcome.guard';


const routes: Routes = [
  {
    path: '', component: HomeActivityComponent,
    canActivate: [WelcomeGuard],
    children: [
      {
        path: 'camera/:lat/:lng/:zoom',
        component: CameraComponent,
        children: [
          {
            path: 'project',
            component: ProjectCreatePlaceholderComponent,
            canActivate: [AuthorizedGuard]
          },
          {
            path: 'event',
            component: EventCreatePlaceholderComponent,
            canActivate: [AuthorizedGuard]
          },
          {
            path: 'inspire',
            component: InspireCreatePlaceholderComponent,
            canActivate: [AuthorizedGuard]
          },
          ...locationRoutes,
        ]
      },
      {
        path: 'profile/:id',
        component: ProfileWrapperComponent,
      }
    ],
  },
  {
    path: 'welcome',
    component: WelcomeActivityComponent,
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    NgxMyDatePickerModule.forRoot(),
    MenuPanelModule,
    PanelMenuModule,
    DialogModule,
    LocationModule,
    TranslateModule.forChild(),
    EventModule,
    InspireModule,
    ProjectModule,
    SliderModule,
    InputTextareaModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    HttpModule,
    ShareButtonsModule.forRoot(),
  ],
  declarations: [
    HomeActivityComponent,
    MapboxNavigatorComponent,
    LoadingIndicatorComponent,
    NotificationComponent,
    InspireCreatePlaceholderComponent,
    EventCreatePlaceholderComponent,
    ProfileEditActivityComponent,
    ProfileViewActivityComponent,
    ProfileWrapperComponent,
    ScoreComponent,
    PromoteThemComponent,
    ProfileComponent,
    GifComponent,
    LocationSearchComponent,
    ProjectMenuComponent,
    UserInfoComponent,
    CameraComponent
  ],
  exports: [
    HomeActivityComponent,
    LoadingIndicatorComponent,
    PanelMenuModule,
    RouterModule
  ],
  providers: [
    AuthorizedGuard,
    WelcomeGuard,
    ProjectService,
    ProjectsApi,
    SessionService,
    UserService,
    UserApi,
    LanguageService,
    PromoteThemService,
    GifService,
    SearchService,
    SearchApi,
    CategoryApi,
    ContextHolderService,
  ]
})
export class HomeModule {
}
