import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LoginCredentialsRequest } from '../../../shared/api/models/logincredentialsrequest';
import { AuthService } from '../../services/auth.service';
import { VALIDATIONS } from '../../constants/validations';
import { ROUTES } from '../../../shared/constants/routes';
import { CONSTANTS } from '../../../shared/constants/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { I18N_ERROR_MESSAGES } from '../../../shared/i18n/elements';
import { validEmail } from '../../helpers/validators';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';


@Component({
  moduleId: module.id,
  selector: 'cd-login-form',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.css']
})
export class LoginFormComponent extends BaseLockComponent implements OnInit {
  public loginForm: FormGroup;
  public credsErrorMessage: any = null;
  public VALIDATIONS = VALIDATIONS;
  public ROUTES = ROUTES;
  public LABELS = CONSTANTS.LOGIN;
  public fromReset: boolean = null;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService
  ) {
    super();
  }

  public ngOnInit() {
    this.buildLoginForm();
    this.loginForm.valueChanges.subscribe(() => this.credsErrorMessage = null);
    this.route.queryParams.subscribe(params => this.fromReset = params.fromReset);
  }

  public onSubmit() {
    if (this.loginForm.valid) {
      this.lockComponent();
      const loginCredentials = new LoginCredentialsRequest(this.loginForm.value);
      this.authService.authenticateUser(loginCredentials)
        .finally(() => this.unlockComponent())
        .subscribe(null,
          (error) => {
            if (error.errorCode === 1) {
              this.translate.get(I18N_ERROR_MESSAGES.credentials)
                .subscribe(message => {
                  this.credsErrorMessage = message;
                });
            }
          });
    }
  }

  public navigateToSignUp() {
    this.router.navigate([this.ROUTES.register]);
  }

  private buildLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', [
        VALIDATIONS.email.required.value && Validators.required,
        Validators.maxLength(this.VALIDATIONS.email.maxLength.value),
        validEmail,
      ]],
      password: ['', [
        VALIDATIONS.password.required.value && Validators.required,
        Validators.minLength(this.VALIDATIONS.password.minLength.value),
        Validators.maxLength(this.VALIDATIONS.password.maxLength.value)],
      ],
      rememberMe: [false],
    });
  }
}
