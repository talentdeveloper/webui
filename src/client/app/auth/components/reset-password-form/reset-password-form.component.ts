import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VALIDATIONS } from '../../constants/validations';
import { controlsMatch, hasAtLeastOneUpper, hasntWhiteSpaces } from '../../helpers/validators';
import { CONSTANTS } from '../../../shared/constants/constants';
import { UserService } from '../../../home/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ResetPasswordRequest } from '../../../shared/api/models/resetpasswordrequest';
import { ROUTES } from '../../../shared/constants/routes';
import { TranslateService } from '@ngx-translate/core';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';

@Component({
  moduleId: module.id,
  selector: 'cd-reset-password-form',
  templateUrl: 'reset-password-form.component.html',
  styleUrls: ['reset-password-form.component.css'],
})
export class ResetPasswordFormComponent extends BaseLockComponent {
  public resetForm: FormGroup;
  public VALIDATIONS = VALIDATIONS;
  public LABELS = CONSTANTS.RESET;
  public showError = false;
  private token: string = null;
  private lang = 'en';

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
  ) {
    super();
    this.buildResetForm();
    this.route.params.subscribe(params => this.token = params.token);
    this.route.queryParams.subscribe(params => {
      this.lang = params.lang;
      this.translate.use(this.lang);
    });
  }

  public onSubmit() {
    this.lockComponent();
    this.userService.resetPassword(this.token, new ResetPasswordRequest(this.resetForm.value))
      .finally(() => this.unlockComponent())
      .subscribe(() => {
        this.router.navigate([ROUTES.login], {queryParams: {fromReset: true}});
      }, () => {
        this.showError = true;
      });
  }

  private buildResetForm() {
    this.resetForm = this.formBuilder.group({
      password: ['', [
        VALIDATIONS.password.required.value && Validators.required,
        Validators.minLength(VALIDATIONS.password.minLength.value),
        Validators.maxLength(VALIDATIONS.password.maxLength.value),
        hasntWhiteSpaces,
        hasAtLeastOneUpper,
      ]],
      confirm: ['', [
        VALIDATIONS.confirmPassword.required.value && Validators.required
      ]],
    }, {
      validator: controlsMatch('password', 'confirm'),
    });
  }
}
