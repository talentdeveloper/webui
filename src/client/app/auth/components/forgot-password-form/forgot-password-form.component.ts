import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VALIDATIONS } from '../../constants/validations';
import { validEmail } from '../../helpers/validators';
import { CONSTANTS } from '../../../shared/constants/constants';
import { UserService } from '../../../home/services/user.service';
import { SendResetPasswordRequest } from '../../../shared/api/models/sendresetpasswordrequest';
import { ROUTES } from '../../../shared/constants/routes';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';

@Component({
  moduleId: module.id,
  selector: 'cd-forgot-password-form',
  templateUrl: 'forgot-password-form.component.html',
  styleUrls: ['forgot-password-form.component.css'],
})
export class ForgotPasswordFormComponent extends BaseLockComponent {
  public forgotForm: FormGroup;
  public VALIDATIONS = VALIDATIONS;
  public LABELS = CONSTANTS.FORGOT;
  public ROUTES = ROUTES;
  public showSuccess = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {
    super();
    this.buildForgotForm();
  }

  public onSubmit() {
    this.lockComponent();
    const requestBody = new SendResetPasswordRequest(this.forgotForm.value);
    this.userService.sendResetPassword(requestBody)
      .finally(() => {
        this.unlockComponent();
        this.showSuccess = true;
      })
      .subscribe();
  }

  private buildForgotForm() {
    this.forgotForm = this.formBuilder.group({
      email: ['', [
        VALIDATIONS.email.required.value && Validators.required,
        Validators.maxLength(this.VALIDATIONS.email.maxLength.value),
        validEmail,
      ]],
    });
  }
}
