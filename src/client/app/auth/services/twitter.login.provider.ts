import { Injectable } from '@angular/core';
import { Dictionary } from '../../shared/types';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { OAuthToken } from '../../shared/api/models/oAuthToken';
import { OAuth1SocialRequest } from '../../shared/api/models/oAuth1SocialRequest';

@Injectable()
export class TwitterLoginProvider {
  static readonly PROVIDER_ID = 'twitter';

  private oauth_token: string;
  private oauth_token_secret: string;
  private popup: any;
  private authorize_url = 'https://api.twitter.com/oauth/authenticate?oauth_token=';

  constructor(private authService: AuthService) {
  }

  public signIn(): Observable<OAuthToken> {
    return new Observable(observer => {
      this.connect()
        .subscribe(res => {
            this.getAccessToken(res)
              .subscribe(res => {
                  observer.next(res);
                },
                (error: any) => {
                  Observable.throw(error);
                });
          },
          (error: any) => {
            this.closePopup();
            Observable.throw(error);
          });
    });
  }

  private connect(): Observable<OAuth1SocialRequest> {
    return new Observable(observer => {
      //Open a blank popup
      this.popup = window.open(null, '_blank', 'height=400,width=800,left=250,top=100,resizable=yes', true);
      //Get an oauth token from the callback url
      return this.getRequestToken()
        .subscribe(response => {
            //Set the OAuth1 Token
            this.oauth_token = response.oauthToken;
            this.oauth_token_secret = response.oauthTokenSecret;
            //Ask the user to authorize the app;
            this.authorize()
              .subscribe((response: any) => {
                  if (!response || !response.oauth_token) {
                    return observer.error('OAuth Token not Found');
                  }

                  //Check if the oauth-token obtained in authorization, matches the original oauth-token
                  if (response.oauth_token !== this.oauth_token) {
                    return observer.error('Invalid OAuth Token received from Twitter.');
                  }

                  return observer.next(new OAuth1SocialRequest({
                    token: {
                      oauth_token: response.oauth_token,
                      oauth_token_secret: this.oauth_token_secret
                    },
                    oauth_verifier: response.oauth_verifier
                  }));
                },
                error => {
                  this.closePopup();
                  return observer.error(error);
                });
          },
          error => {
            this.closePopup();
            return observer.error(error);
          });
    });
  }

  private closePopup(): void {
    if (this.popup && !this.popup.closed) {
      this.popup.close();
    }
  }

  private getUrlQueryObject(query_string: string): Dictionary<any> {
    const vars: Dictionary<any> = {};
    let hash: string[];
    if (!query_string) {
      return null;
    }
    const hashes: string[] = query_string.slice(1).split('&');
    for (let i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars[hash[0]] = hash[1];
    }
    return vars;
  }

  private getRequestToken(): Observable<OAuthToken> {
    return this.authService.getRequestToken(TwitterLoginProvider.PROVIDER_ID)
      .map(res => res)
      .catch(error => Observable.throw(error))
      .share();
  }

  private getAccessToken(data: any): Observable<OAuthToken> {
    return this.authService.getAccessToken(TwitterLoginProvider.PROVIDER_ID, data)
      .map(res => res)
      .catch(error => Observable.throw(error))
      .share();
  }

  private authorize(): Observable<Dictionary<any>> {
    return new Observable(observer => {
      if (!this.popup) {
        observer.error('Popup Not initialized');
      }
      this.popup.location.href = this.authorize_url + this.oauth_token;
      const wait = () => {
        setTimeout(() => {
          return this.popup.closed ? observer.next(this.getUrlQueryObject(this.popup.location.search)) : wait();
        }, 25);
      };
      wait();
    });
  }

}
