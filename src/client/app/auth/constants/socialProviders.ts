export enum socialProviders {
  FACEBOOK = 0,
  GOOGLE = 1,
  TWITTER = 2,
}
