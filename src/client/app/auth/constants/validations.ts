export const CAPACITY = {
  required: {
    value: false
  },
  minValue: {
    value: 0,
    errorMessage: 'Capacity can\'t be negative'
  },
  maxValue: {
    value: 1000000,
    errorMessage: 'Capacity can\'t be more than 1000000'
  },
  maxLength: {
    value: 6
  }
};

export const VALIDATIONS = {
  search: {
    query: {
      required: {
        value: true,
        errorMessage: 'Search query is required',
      }
    },
  },
  notifications: {
    required: {
      value: true,
    },
  },
  gender: {
    required: {
      value: false,
    },
  },
  language: {
    required: {
      value: true,
    }
  },
  firstName: {
    required: {
      value: true,
      errorMessage: 'First name is required',
    },
    valid: {
      errorMessage: 'First name is not valid',
    },
    maxLength: {
      value: 256,
      errorMessage: 'First name maximal length is 256',
    },
  },
  lastName: {
    required: {
      value: true,
      errorMessage: 'Last name is required'
    },
    valid: {
      errorMessage: 'Last name is not valid',
    },
    maxLength: {
      value: 256,
      errorMessage: 'Last name maximal length is 256',
    },
  },
  email: {
    required: {
      value: true,
      errorMessage: 'Email is required',
    },
    maxLength: {
      value: 1024,
      errorMessage: 'Email maximal length is 1024',
    },
    regex: {
      value: new RegExp(/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/),
      errorMessage: 'Email is not valid',
    },
  },
  password: {
    required: {
      value: true,
      errorMessage: 'Password is required',
    },
    valid: {
      errorMessage: 'Password is not valid',
    },
    uppercase: {
      errorMessage: 'Password should contain 1 uppercase letter'
    },
    minLength: {
      value: 8,
      errorMessage: 'Password minimal length is 8',
    },
    maxLength: {
      value: 128,
      errorMessage: 'Password maximal length is 128',
    },
  },
  confirmPassword: {
    required: {
      value: true,
      errorMessage: 'Confirm password is required'
    },
    match: {
      errorMessage: 'Passwords doesn\'t match',
    },
  },
  newPassword: {
    required: {
      value: true,
      errorMessage: 'New password is required',
    },
    valid: {
      errorMessage: 'New password is not valid',
    },
    uppercase: {
      errorMessage: 'New password should contain 1 uppercase letter'
    },
    minLength: {
      value: 8,
      errorMessage: 'Password minimal length is 8',
    },
    maxLength: {
      value: 128,
      errorMessage: 'Password maximal length is 128',
    },
  },
  birthDate: {
    required: {
      value: false,
    },
    maxYear: {
      value: 16,
      errorMessage: 'This date of birth cannot be accepted.'
    },
    greaterThanToday: {
      errorMessage: 'Date shouldn\'t be greater than today',
    },
  },
  city: {
    required: {
      value: false,
    },
    valid: {
      errorMessage: 'City is not valid',
    },
    maxLength: {
      value: 128,
      errorMessage: 'City max length is 128',
    },
  },
  address: {
    required: {
      value: false,
    },
    valid: {
      errorMessage: 'Address is not valid'
    },
    maxLength: {
      value: 1024,
      errorMessage: 'Address max length is 1024',
    },
  },
  terms: {
    required: {
      value: true,
    },
    agree: {
      errorMessage: 'I agree with terms and conditions'
    },
    shouldAgree: {
      errorMessage: 'You should agree with terms and conditions',
    },
  },
  project: {
    name: {
      required: {
        value: true,
        errorMessage: 'Name is required'
      },
      maxLength: {
        value: 2048,
        errorMessage: 'Project name can\'t be greater than 2048 symbols'
      }
    },
    description: {
      maxLength: {
        value: 2048,
        errorMessage: 'Project details can\'t be greater than 2048 symbols'
      },
      description: {
        required: false,
      }
    },
    rotation: {
      required: {
        value: true
      }
    },
    scale: {
      required: {
        value: true
      }
    },
    capacity: CAPACITY,
    category: {
      required: {
        value: true,
      }
    },
  },
  event: {
    name: {
      required: {
        value: true,
        errorMessage: 'Name is required',
      },
      maxLength: {
        value: 2048,
        errorMessage: 'Event name can\'t be greater than 2048 symbols',
      },
    },
    description: {
      maxLength: {
        value: 4096,
        errorMessage: 'Event details can\'t be greater than 4096 symbols',
      },
      required: {
        value: false,
      }
    },
    startDate: {
      required: {
        value: true,
        errorMessage: 'Start date is required',
      },
      timeInterval: {
        errorMessage: 'Start date should be greater than today date',
      }
    },
    endDate: {
      required: {
        value: true,
        errorMessage: 'End date is required'
      },
      timeInterval: {
        errorMessage: 'End date should be greater than start date',
      }
    },
    capacity: CAPACITY
  },
  inspire: {
    name: {
      required: {
        value: true,
        errorMessage: 'Name is required',
      },
      maxLength: {
        value: 1024,
        errorMessage: 'Idea name can\'t be greater than 1024 symbols',
      },
    },
    description: {
      required: {
        value: true,
        errorMessage: 'Details are required',
      },
      maxLength: {
        value: 12288,
        errorMessage: 'Idea details can\'t be greater than 12288 symbols',
      },
    },
    capacity: CAPACITY
  }
};
