export const LANGUAGES = [
  {
    label: 'English',
    value: 'en',
  },
  {
    label: 'Русский',
    value: 'ru',
  },
  {
    label: 'Français',
    value: 'fr',
  },
  {
    label: 'Deutsche',
    value: 'de',
  },
  {
    label: 'Español',
    value: 'es',
  },
  {
    label: 'Italiano',
    value: 'it',
  },
];
