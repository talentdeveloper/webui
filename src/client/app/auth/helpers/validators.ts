import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';
import { VALIDATIONS } from '../constants/validations';

export function validEmail(control: AbstractControl) {
  const email = control.value;
  if (!VALIDATIONS.email.regex.value.test(email)) {
    return {
      validEmail: true,
    };
  } else {
    return null;
  }
}

export function hasAtLeastOneUpper(control: AbstractControl) {
  const password = control.value;
  if (!/[A-Z]+/.test(password)) {
    return {
      hasAtLeastOneUpper: true,
    };
  } else {
    return null;
  }
}

export function hasntWhiteSpaces(control: AbstractControl) {
  const str: string = control.value;
  if (str) {
    if (str.indexOf(' ') >= 0) {
      return {
        hasntWhiteSpaces: true,
      };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function containsWhiteSpacesOnly(control: AbstractControl) {
  const str: string = control.value;
  if (str) {
    if (!str.replace(/\s/g, '').length && str.includes(' ')) {
      return {
        containsWhiteSpacesOnly: true,
      };
    } else {
      return null;
    }
  }
  return null;
}

export function dateIsntGreaterThanToday(control: AbstractControl) {
  if (!control.value) {
    return null;
  }
  if (new Date(control.value) > new Date()) {
    return {
      dateIsntGreaterThanToday: true,
    };
  } else {
    return null;
  }
}
export function controlsMatch(firstControlName: string, secondControlName: string): ValidatorFn {
  return function(formGroup: FormGroup) {
    const firstControl = formGroup.get(firstControlName);
    const secondField = formGroup.get(secondControlName);
    if (firstControl.value !== secondField.value) {
      secondField.setErrors({controlsMatch: true});
    } else {
      return null;
    }
  };
}
