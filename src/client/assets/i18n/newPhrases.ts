/**
 * That File uses for gulp task 'translation.gen' in order to generate translation for each available language in app.
 * Please, clear that list if those phrases already in translation files.
 * @type {[string]}
 */

export const phrases: string[] = [];
