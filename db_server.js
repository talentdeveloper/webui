const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);
server.use(jsonServer.bodyParser);
// Add custom routes before JSON Server router
server.post('/login', (req, res) => {
  res.jsonp({
    service: {
      successful: true,
      node_id: 'a',
      api_version: 1,
      error_code: 0,
      error_message: 'no error',
      validation_errors: 'no error',
      verbose_error_message: 'no error'
    },
    payload: {
      id: 'dcae216c-b0fe-4c28-8ace-fb6ea12bdfcf',
      email: 'john.doe@example.com',
      firstName: 'John',
      lastName: 'Doe',
      birthDate: 'Mon Nov 06 2017 13:51:01 GMT+0200 (EET)',
      gender: 'm',
      nationality: 'France',
      address: 'Karbysheva st.'
    }
  });
});

server.post('/signup', (req, res) => {
  res.jsonp({
    service: {
      successful: false,
      node_id: 'a',
      api_version: 1,
      error_code: 0,
      error_message: 'no error',
      validation_errors: {
        'password': ['Ensure this field has at least 8 characters.'],
      },
      verbose_error_message: 'no error'
    },
    payload: req.body,
  })
});

server.get('/session', (req, res) => {
  res.jsonp({
    service: {
      successful: true,
      node_id: 'a',
      api_version: 1,
      error_code: 0,
      error_message: 'no error',
      validation_errors: {
        'password': ['Ensure this field has at least 8 characters.'],
      },
      verbose_error_message: 'no error'
    },
    payload: null
  })
});

router.render = (req, res) => {
  res.jsonp({
    service: {
      successful: true,
      node_id: 'a',
      api_version: 1,
      error_code: 0,
      error_message: 'no error',
      validation_errors: 'no error',
      verbose_error_message: 'no error'
    },
    payload: res.locals.data
  })
};

server.use((req, res, next) => {
  res.header('content-type', 'application/json');
  // Continue to JSON Server router
  next()
});

// Use default router
server.use(router);
server.listen(3000, () => {
  console.log('JSON Server is running')
});
