### Bootstrapped from [Angular-seed](https://github.com/mgechev/angular-seed)


## Project setup
1. Make sure that **node 6.11.1** and **npm**, packages are installed
2. Run ```npm install```

## Production deployment Steps
1. Merge branch [development] into [master] branch 
2. Run **Build & Deploy PROD // WebUI** Teamcity Build Configuration
3. Change Content-Type for .svg files in S3 to image/svg+xml
4. Create new CloudFront invalidation for path ```/*```  

## Install Cordova
For Android: https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html

For iOS: https://cordova.apache.org/docs/en/latest/guide/platforms/ios/index.html

## Cordova application
1. Go to: ```cd cordova/```
2. Make WWW directory: ```mkdir www```
3. Prepare env: ```npm run cordova.android.prepare.ci.env``` or ```npm run cordova.android.prepare.prod.env```
4. Run cordova app: before - connect mobile device, after - ```npm run cordova.build.ci.android``` (apk will be to install to device automatically)
5. Build cordova app: ```npm run cordova.build.prod.android```

### Generate icons
1. Go to: ```cd cordova/```
2. Run: ```npm run resgen.android.icon``` or ```npm run resgen.ios.icon```
