import { EnvConfig } from './env-config.interface';

const BaseConfig: EnvConfig = {
  NAVIGATOR_SETTINGS: {
    TOKEN: 'pk.eyJ1Ijoia25vcnJ1cyIsImEiOiJjamhocDc5c2oxcHZmM2NvNnlyMHg3YW16In0.tVCD-Tywf6WWSUCTt3EeHw',
    STYLE: 'mapbox://styles/mapbox/streets-v10',
    MIN_ZOOM: 1,
    MAX_ZOOM: 20
  }
};

export = BaseConfig;

