const hbs = require('handlebars');
import * as _ from 'lodash';

export default class CodegenUtils {

  public static mapDefinitionsFromModel(model: any): any {
    const definitionMap: any = {};
    if (model && model.definitions) {
      for (const definitionName of Object.keys(model.definitions)) {
        const definitionKey = '#/definitions/' + definitionName;
        const currentDef = model.definitions[definitionName];
        if (currentDef.allOf && currentDef.allOf.length) {
          const properties = {};
          for (const item of currentDef.allOf) {
            if (item.$ref) {
              const ref = item.$ref.split('/');
              const def = model.definitions[ref[ref.length - 1]];
              if (def.properties) {
                _.assign(properties, def.properties);
              }
            }
            if (item.properties) {
              _.assign(properties, item.properties);
            }
          }
          currentDef.properties = properties;
        }
        currentDef.definitionName = definitionName;
        currentDef.referencePath = definitionKey;
        definitionMap[definitionKey] = currentDef;
      }
    }
    return definitionMap;
  }

  public static registerImportReference(className: string, context: any) {
    const targetObject = context.data.root;
    targetObject.references = targetObject.references || [];
    if (targetObject.references.indexOf(className) < 0) {
      targetObject.references.push(className);
    }
  }

  public static resolvePropertyName(fieldName: string): string {
    return _.camelCase(fieldName);
  }

  public static resolveRef(ref: string, definitionMap: any): any {
    const resolvedRef = definitionMap[ref];
    if (!resolvedRef) {
      throw new Error('Unknown type: ' + resolvedRef);
    }
    return resolvedRef;
  }

  public static fileNameForClass(className: string) {
    return className.toLowerCase();
  }

  public static resolveTagName(className: string, context: any) {
    const tagName = context.data.root.members[0].tags[0];
    if (!tagName) {
      throw new Error('All methods should have tags parameter (Array of string)');
    }
    return tagName.toLowerCase();
  }

  public static resolveRefClassName(ref: any, definitionMap: any) {
    const resolvedRef = CodegenUtils.resolveRef(ref, definitionMap);
    return resolvedRef.definitionName;
  }

  public static resolveTypescriptType(typeDef: any, context: any): string {
    if (!typeDef.type && !typeDef.$ref && !typeDef.schema) {
      throw new Error('Invalid type definition ' + JSON.stringify(typeDef));
    }
    const definitionMap = context.data.root.definitionMap;
    const swaggerType = typeDef.type;
    switch (swaggerType) {
      case 'integer':
      case 'double':
      case 'int':
      case 'int32':
      case 'number':
        return 'number';
      case 'boolean':
        return 'boolean';
      case 'string':
        return 'string';
      case 'array':
        if (!typeDef.items) {
          throw new Error('Array should have specified type of items.');
        }
        let itemsTypeName;
        typeDef._isArrayDto = true;
        if (!typeDef.items.$ref) {
          if (typeDef.items.type) {
            itemsTypeName = typeDef.items.type;
          } else {
            throw new Error('Array should have specified type of items.');
          }
        } else {
          itemsTypeName = CodegenUtils.resolveRefClassName(typeDef.items.$ref, definitionMap);
          CodegenUtils.registerImportReference(itemsTypeName, context);
        }
        return new hbs.SafeString('Array<' + itemsTypeName + '>');
      case 'object':
        if (typeDef.allOf && typeDef.allOf[0].$ref === '#/definitions/ApiResponse') {
          // tslint:disable-next-line
          return new hbs.SafeString('ApiResponseObservable<' + CodegenUtils.resolveTypescriptType(typeDef.allOf[1].properties.payload, context) + '>');
        } else if (typeDef.schema && typeDef.schema.$ref) {
          typeDef._isRefDto = true;
          return CodegenUtils.resolveTypescriptType(typeDef.schema, context);
        } else {
          return 'any';
        }
      default:
        if ((typeDef.schema && typeDef.schema.$ref) || typeDef.$ref) {
          const ref = typeDef.$ref || typeDef.schema.$ref;
          const itemsTypeName = CodegenUtils.resolveRefClassName(ref, definitionMap);
          CodegenUtils.registerImportReference(itemsTypeName, context);
          typeDef._isRefDto = true;
          return new hbs.SafeString(itemsTypeName);
        } else {
          return 'any';
        }
    }
  }

  public static resolveClassName(definitionName: string, context: any): string {
    return _.upperFirst(_.camelCase(definitionName));
  }

  public static operationNameForController(methodDef: any) {
    if (!methodDef.operationId) {
      throw new Error('All methods should have operationId parameter ()');
    }
    return methodDef.operationId;
  }

}
