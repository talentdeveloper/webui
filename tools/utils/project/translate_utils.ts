import * as _ from 'lodash';
import { phrases } from '../../../src/client/assets/i18n/newPhrases';

const request = require('request-promise');

export default class TranslateUtils {

  public static translate(targetLanguage: string): any {
    const googleTranslate = (config: any) => {
      let text, sourceLang, targetLang;
      text = config.text;
      sourceLang = config.source;
      targetLang = config.target;

      if (!text) {
        throw new Error('Need a text to translate');
      }
      if (!targetLang) {
        throw new Error('Need a targetLang to translate to');
      }
      const url = 'https://translate.google.com/translate_a/single'
        + '?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=es-ES&ie=UTF-8'
        + '&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e';

      const data = {
        'sl': sourceLang,
        'tl': targetLang,
        'q': text,
      };

      return request({
        method: 'POST',
        uri: url,
        encoding: 'UTF-8',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
          'User-Agent': 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1',
        },
        form: data,
        json: true,
      }).then(function (json: any) {
        return json.sentences.reduce((combined: any, trans: any) => combined + (trans.trans || ''), '');
      }).catch(function () {
        throw new Error('Couldn\'t retrieve a valid JSON response. Perhaps the API has changed, please let us know.');
      });
    };

    const result: any = {};
    const promises: any = [];
    _.each(phrases, (phrase) => {
      const promise = googleTranslate({
        text: phrase,
        source: 'en',
        target: targetLanguage
      }).then((res: string) => {
        result[phrase] = res;
      });
      promises.push(promise);
    });
    return Promise.all(promises)
      .then(() => {
        return result;
      });
  }
}
