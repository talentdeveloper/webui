import { join } from 'path';
import { SeedConfig } from './seed.config';
import { ExtendPackages } from './seed.config.interfaces';


/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {
  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

  // Enable SCSS
  ENABLE_SCSS = true;

  // external fonts
  FONTS_DEST = `${this.APP_DEST}/fonts`;

  FONTS_SRC = [
    'node_modules/font-awesome/fonts/**',
    `${this.APP_SRC}/assets/fonts/**`
  ];

  // Cordova
  CORDOVA = 'cordova';
  CORDOVA_CONFIG = 'cordova/config';
  CORDOVA_WWW = 'cordova/www';
  IS_CORDOVA = process.argv.indexOf('--cordova') > -1;

  // PrimeNg
  PRIME_NG_THEME = 'flick';
  CSS_IMAGE_DEST = `${this.CSS_DEST}/images`;
  CSS_IMAGE_SRC = [
    'node_modules/primeng/resources/themes/' + this.PRIME_NG_THEME + '/images/**'
  ];
  THEME_FONTS_DEST = `${this.APP_DEST}/css/fonts`;
  THEME_FONTS_SRC = [
    'node_modules/primeng/resources/themes/' + this.PRIME_NG_THEME + '/fonts/**',
  ];

  constructor() {
    super();
    this.APP_TITLE = 'Codemos';
    // this.GOOGLE_ANALYTICS_ID = 'Your site's ID';

    /* Enable typeless compiler runs (faster) between typed compiler runs. */
    // this.TYPED_COMPILE_INTERVAL = 5;

    // Add `NPM` third-party libraries to be injected/bundled.
    this.NPM_DEPENDENCIES = [
      ...this.NPM_DEPENDENCIES,
      {src: 'primeng/resources/primeng.css', inject: true},
      {src: 'font-awesome/css/font-awesome.min.css', inject: true},
      {src: 'primeng/resources/themes/flick/theme.css', inject: true},
      {src: 'chart.js/dist/Chart.js', inject: 'libs'},
      {src: 'moment/moment.js', inject: true},
      {src: 'css-star-rating/dist/css/star-rating.css', inject: true},
      {src: '@ng-select/ng-select/themes/default.theme.css', inject: true},
      {src: 'mapbox-gl/dist/mapbox-gl.css', inject: true},
      {src: 'mapbox-gl/dist/mapbox-gl.js', inject: true}
    ];

    // Add `local` third-party libraries to be injected/bundled.
    this.APP_ASSETS = [
      // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
      // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
    ];

    this.ROLLUP_INCLUDE_DIR = [
      ...this.ROLLUP_INCLUDE_DIR,
      //'node_modules/moment/**'
    ];

    this.ROLLUP_NAMED_EXPORTS = [
      ...this.ROLLUP_NAMED_EXPORTS,
      //{'node_modules/immutable/dist/immutable.js': [ 'Map' ]},
    ];

    const additionalPackages: ExtendPackages[] = [
      {
        name: '@ngx-translate/core',
        path: 'node_modules/@ngx-translate/core/bundles/core.umd.js',
      },
      {
        name: '@ngx-translate/http-loader',
        path: 'node_modules/@ngx-translate/http-loader/bundles/http-loader.umd.js',
      },
      {
        name: 'primeng',
        path: 'node_modules/primeng',
        packageMeta: {main: 'primeng.js', defaultExtension: 'js'}
      },
      {
        name: 'lodash',
        path: 'node_modules/lodash',
        packageMeta: {main: 'lodash.js', defaultExtension: 'js'}
      },
      {
        name: 'ngx-mydatepicker',
        path: 'node_modules/ngx-mydatepicker',
      },
      {
        name: 'ngx-sharebuttons',
        path: 'node_modules/ngx-sharebuttons',
      },
      {
        name: '@ng-select/ng-select',
        path: 'node_modules/@ng-select/ng-select/bundles/ng-select.umd.js'
      },
      {
        name: 'moment',
        path: 'node_modules/moment',
        packageMeta: {main: 'moment.js', defaultExtension: 'js'},
      },
      {
        name: 'mapbox-gl',
        path: 'node_modules/mapbox-gl/dist/mapbox-gl.js'
      },
      {
        name: 'progressbar.js',
        path: 'node_modules/progressbar.js',
      },
      {
        name: 'angular5-social-login',
        path: 'node_modules/angular5-social-login/angular5-social-login.umd.js'
      }
    ];
    this.addPackagesBundles(additionalPackages);

    /* Add proxy middleware */
    // this.PROXY_MIDDLEWARE = [
    //   require('http-proxy-middleware')('/api', { ws: false, target: 'http://localhost:3003' })
    // ];

    /* Add to or override NPM module configurations: */
    // this.PLUGIN_CONFIGS['browser-sync'] = { ghostMode: false };
  }
}
