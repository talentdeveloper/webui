import { clean } from '../../utils/seed/clean';
import Config from '../../config';
import { join } from 'path';

export = clean([
  join(Config.CORDOVA, 'node_modules'),
  join(Config.CORDOVA, 'platforms'),
  join(Config.CORDOVA, 'plugins'),
  join(Config.CORDOVA, '*.env'),
  join(Config.CORDOVA, 'config.xml'),
  join(Config.CORDOVA, 'package.json'),
]);
