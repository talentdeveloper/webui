import * as gulp from 'gulp';
import { exec } from 'child_process';

export = () => {
  gulp.task('local.db', () => {
    const child = exec('node db_server.js');

    child.stdout.on('data', function(data) {
      console.log('Out from localdb: ' + data);
    });
    child.stderr.on('data', function(data) {
      console.log('Err from lcoaldb: ' + data);
    });
    child.on('close', function(code) {
      console.log('closing localdb: ' + code);
    });
  });
};
