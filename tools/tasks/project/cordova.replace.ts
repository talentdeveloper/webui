import * as gulp from 'gulp';
import { TemplateLocalsBuilder } from '../../utils';
import { argv } from 'yargs';
import * as fs from 'fs';
import Config from '../../config';
import { join } from 'path';

const replace = require('gulp-replace');

export = () => {
  const config = new TemplateLocalsBuilder().withStringifiedSystemConfigDev().build();
  const envConfig = JSON.parse(config.ENV_CONFIG);
  if (!argv['build-number']) {
    throw new Error('You have to provide buildNumber as : -- --build-number');
  }

  fs.writeFileSync(join(Config.CORDOVA, [argv['env-config'], '.env'].join('')), argv['env-config']);

  const googleKeys = envConfig[argv['platform'].toUpperCase()];

  return gulp.src([join(Config.CORDOVA_CONFIG, 'package.json'),
                   join(Config.CORDOVA_CONFIG, 'config.xml')])
    .pipe(replace('${VERSION}', envConfig.VERSION))
    .pipe(replace('${BUILD_NUMBER}', argv['build-number']))
    .pipe(replace('${FACEBOOK_CLIENT_ID}', envConfig.FACEBOOK_CLIENT_ID))
    .pipe(replace('${FACEBOOK_APP_NAME}', envConfig.FACEBOOK_APP_NAME))
    .pipe(replace('${GOOGLE_REVERSED_CLIENT_ID}', googleKeys.GOOGLE_REVERSED_CLIENT_ID))
    .pipe(replace('${GOOGLE_CLIENT_ID}', googleKeys.GOOGLE_CLIENT_ID))
    .pipe(replace('${MAPBOX_TOKEN}', envConfig.NAVIGATOR_SETTINGS.TOKEN))
    .pipe(gulp.dest(Config.CORDOVA));
};
